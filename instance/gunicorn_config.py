import multiprocessing
import sys

# bind = 'unix:/run/pbxapi_lb_api-gunicorn.sock'
bind = '127.0.0.1:8000'
# workers = multiprocessing.cpu_count()
workers = 1
worker_class = 'uvicorn.workers.UvicornWorker'
# worker_connections = 1000
# timeout = 30
# keepalive = 2
# spew = False
daemon = False
# pidfile = '/run/pbxapi_lb_api-gunicorn.pid'
# user = 'www-data'
# group = 'www-data'
tmp_upload_dir = None
# proc_name = 'pbxapi_lb_api_gunicorn'

log_level = 'DEBUG'
log_level_panoramisk = 'INFO'
# chdir =
raw_env = [
  'PYTHONUNBUFFERED=1',
  'CONFIG_FILE=../instance/config.yml',
  'PYTHONASYNCIODEBUG=1',
  'DEBUG=1'
]
# raw_env = [
#   'PYTHONUNBUFFERED=0',
#   'CONFIG_FILE=../instance/config.yml',
#   'PYTHONASYNCIODEBUG=0',
#   'DEBUG=0'
# ]


logconfig_dict = {
  'version': 1,
  'formatters': {
    'console': {
      '()': 'api.core.logger_formatters.CorrelationIdFormatter',
      'format': '%(asctime)s PBXAPI-LB-API-%(name)s [%(process)d] [%(request_id)s] [%(correlation_id)s] %(levelname)s: %(message)s'
    },
    'syslog': {
      '()': 'api.core.logger_formatters.CorrelationIdFormatter',
      'format': 'PBXAPI-LB-API-%(name)s [%(process)d] [%(request_id)s] [%(correlation_id)s] %(levelname)s: %(message)s'
    }
  },
  'filters': {
    'exclude_warning': {
      '()': 'api.core.logger_formatters.ExcludeWarningFilter',
    },
    'include_warning': {
      '()': 'api.core.logger_formatters.IncludeWarningFilter',
    },
  },
  'handlers': {
    'console': {
      'class': 'logging.StreamHandler',
      'formatter': 'console',
    },
    'console_stdout': {
      'class': 'logging.StreamHandler',
      'formatter': 'console',
      'filters': ['exclude_warning'],
      'stream': sys.stdout,
    },
    'console_stderr': {
      'class': 'logging.StreamHandler',
      'formatter': 'console',
      'filters': ['include_warning'],
      'stream': sys.stderr,
    },
    'syslog': {
      'class': 'logging.handlers.SysLogHandler',
      'address': '/dev/log',
      'formatter': 'syslog',
    }
  },
  'loggers': {
    'generic': {
      'level': log_level,
      'handlers': [
        'console_stdout', 'console_stderr', 'syslog'
      ],
      'propagate': False,
    },
    'gunicorn': {
      'level': log_level,
      'handlers': [
        'console_stdout', 'console_stderr', 'syslog'
      ],
      'propagate': False,
    },
    'gunicorn.access': {
      'level': log_level,
      'handlers': [
        'console_stdout', 'console_stderr', 'syslog'
      ],
      'propagate': False,
    },
    'panoramisk': {
      'level': log_level_panoramisk,
      'handlers': [
        'console_stdout', 'console_stderr', 'syslog'
      ],
      'propagate': False,
    },
    'root': {
      'level': log_level,
      'handlers': [
        'console'
      ],
      'propagate': False,
    }
  }
}