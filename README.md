CRM-side component. Intended to provide methods for CRM to initialize phone calls, manipulate with 
queue members, get active phone calls data (TODO) and others (depends on further backlog tasks
of CRM developers).

This project is not in the release stage, but it works. Use PyCharm to import project. PyCharm must include 
docker and docker-compose integration. Python 3.8 version required. 
Run `pip install -r ./requirements/development.txt` to prepare virtualenv for the project.

Project includes a docker files for postgresql database and rabbitmq queues. Use docker-compose 
pycharm configuration to start a testing environment and gunicorn run configuration to run API itself.

Project includes gitlab-ci script to deploy in manual mode (it needs to trigger deployment action in 
gitlab after committing into testing or production branches). It needs to restart systemd unit 
pbxapi_lb_api.service and socket pbxapi_lb_api-gunicorn.socket to apply changes after completed 
triggered deployment.

Minimal architecture for this project on the nodes, where API will be deployed, is:
1. Nginx to proxyfying requests into systemd created socket (nginx configuration file for 
   subdirectory sites-available located in deploy/__data/nginx.conf - must be configurated manually),
2. Ansible 2.7+ (better - latest version) to deploying routines (gitlab-ci runs ansible playbook),
3. PostgreSQL 12 (not in cluster, isolated instance) on the every node, database and schema needs to be 
   created manually (look into subdirectory "docker_initdb" in dockerfiles/docker_initdb),
4. RabbitMQ (latest version) on the every node, definition files may be found in subdirectory "rabbitmq"
   in dockerfiles. RabbitMQ *must* be configured as cluster.
   
Also, asterisk nodes should be available for AMI connection from every node, where this API deployed.
It needs to create manager.conf user on the every asterisk to node with the minimal permission: 
write=originate.

Initial install routines on the API node can be done by running script install_to_localhost.sh from deploy 
directory (run this script *after* cd into "deploy" directory). Before that configuration of ansible variables in 
deploy/ansible_inventory is required to provide usernames and passwords for all users including: PostgreSQL,
RabbitMQ, Asterisk AMI interface.

This project was developed for running under Ubuntu Server with version 16 and later.