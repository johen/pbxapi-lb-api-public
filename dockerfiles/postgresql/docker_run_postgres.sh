#!/bin/bash

docker_tag=johen/pbxapi-lb-listener_postgres
docker_container=pbxapi-lb-listener_postgres

docker build -t $docker_tag -f ./Dockerfile .
docker run -i --rm -p 5432:5432 \
-v $PWD/postgresql.conf:/etc/postgresql/postgresql.conf \
-v $PWD/docker_initdb:/docker-entrypoint-initdb.d \
--name $docker_container \
$docker_tag \
postgres -c config_file=/etc/postgresql/postgresql.conf