--
-- PostgreSQL database dump
--

-- Dumped from database version 12.6 (Debian 12.6-1.pgdg100+1)
-- Dumped by pg_dump version 12.6 (Ubuntu 12.6-1.pgdg20.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: pbx_ra; Type: DATABASE; Schema: -; Owner: pbx_ra_user
--

CREATE DATABASE pbx_ra WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.utf8' LC_CTYPE = 'en_US.utf8';


ALTER DATABASE pbx_ra OWNER TO pbx_ra_user;

\connect pbx_ra

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: v1; Type: SCHEMA; Schema: -; Owner: pbx_ra_user
--

CREATE SCHEMA v1;


ALTER SCHEMA v1 OWNER TO pbx_ra_user;

--
-- Name: queue_autopause_values; Type: TYPE; Schema: v1; Owner: pbx_ra_user
--

CREATE TYPE v1.queue_autopause_values AS ENUM (
    'yes',
    'no',
    'all'
);


ALTER TYPE v1.queue_autopause_values OWNER TO pbx_ra_user;

--
-- Name: queue_members_event_values; Type: TYPE; Schema: v1; Owner: pbx_ra_user
--

CREATE TYPE v1.queue_members_event_values AS ENUM (
    'added',
    'deleted'
);


ALTER TYPE v1.queue_members_event_values OWNER TO pbx_ra_user;

--
-- Name: queue_strategy_values; Type: TYPE; Schema: v1; Owner: pbx_ra_user
--

CREATE TYPE v1.queue_strategy_values AS ENUM (
    'ringall',
    'leastrecent',
    'fewestcalls',
    'random',
    'rrmemory',
    'linear',
    'wrandom',
    'rrordered'
);


ALTER TYPE v1.queue_strategy_values OWNER TO pbx_ra_user;

--
-- Name: yesno_values; Type: TYPE; Schema: v1; Owner: pbx_ra_user
--

CREATE TYPE v1.yesno_values AS ENUM (
    'yes',
    'no'
);


ALTER TYPE v1.yesno_values OWNER TO pbx_ra_user;

--
-- Name: queue_members_uniqueid_seq; Type: SEQUENCE; Schema: v1; Owner: pbx_ra_user
--

CREATE SEQUENCE v1.queue_members_uniqueid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE v1.queue_members_uniqueid_seq OWNER TO pbx_ra_user;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: queue_members; Type: TABLE; Schema: v1; Owner: pbx_ra_user
--

CREATE TABLE v1.queue_members (
    queue_name character varying(80) NOT NULL,
    interface character varying(80) NOT NULL,
    membername character varying(80),
    state_interface character varying(80),
    penalty integer,
    paused integer,
    uniqueid integer DEFAULT nextval('v1.queue_members_uniqueid_seq'::regclass) NOT NULL,
    wrapuptime integer,
    static_membership boolean DEFAULT false NOT NULL,
    event v1.queue_members_event_values DEFAULT 'added'::v1.queue_members_event_values NOT NULL,
    correlation_id uuid,
    nodename character varying(64)
);


ALTER TABLE v1.queue_members OWNER TO pbx_ra_user;

--
-- Name: queue_rules; Type: TABLE; Schema: v1; Owner: pbx_ra_user
--

CREATE TABLE v1.queue_rules (
    rule_name character varying(80) NOT NULL,
    "time" character varying(32) NOT NULL,
    min_penalty character varying(32) NOT NULL,
    max_penalty character varying(32) NOT NULL
);


ALTER TABLE v1.queue_rules OWNER TO pbx_ra_user;

--
-- Name: queues; Type: TABLE; Schema: v1; Owner: pbx_ra_user
--

CREATE TABLE v1.queues (
    name character varying(128) NOT NULL,
    musiconhold character varying(128),
    announce character varying(128),
    context character varying(128),
    timeout integer,
    ringinuse v1.yesno_values,
    setinterfacevar v1.yesno_values,
    setqueuevar v1.yesno_values,
    setqueueentryvar v1.yesno_values,
    monitor_format character varying(8),
    membermacro character varying(512),
    membergosub character varying(512),
    queue_youarenext character varying(128),
    queue_thereare character varying(128),
    queue_callswaiting character varying(128),
    queue_quantity1 character varying(128),
    queue_quantity2 character varying(128),
    queue_holdtime character varying(128),
    queue_minutes character varying(128),
    queue_minute character varying(128),
    queue_seconds character varying(128),
    queue_thankyou character varying(128),
    queue_callerannounce character varying(128),
    queue_reporthold character varying(128),
    announce_frequency integer,
    announce_to_first_user v1.yesno_values,
    min_announce_frequency integer,
    announce_round_seconds integer,
    announce_holdtime character varying(128),
    announce_position character varying(128),
    announce_position_limit integer,
    periodic_announce character varying(50),
    periodic_announce_frequency integer,
    relative_periodic_announce v1.yesno_values,
    random_periodic_announce v1.yesno_values,
    retry integer,
    wrapuptime integer,
    penaltymemberslimit integer,
    autofill v1.yesno_values,
    monitor_type character varying(128),
    autopause v1.queue_autopause_values,
    autopausedelay integer,
    autopausebusy v1.yesno_values,
    autopauseunavail v1.yesno_values,
    maxlen integer,
    servicelevel integer,
    strategy v1.queue_strategy_values,
    joinempty character varying(128),
    leavewhenempty character varying(128),
    reportholdtime v1.yesno_values,
    memberdelay integer,
    weight integer,
    timeoutrestart v1.yesno_values,
    defaultrule character varying(128),
    timeoutpriority character varying(128)
);


ALTER TABLE v1.queues OWNER TO pbx_ra_user;

--
-- Name: queue_members queue_members_pkey; Type: CONSTRAINT; Schema: v1; Owner: pbx_ra_user
--

ALTER TABLE ONLY v1.queue_members
    ADD CONSTRAINT queue_members_pkey PRIMARY KEY (queue_name, interface);


--
-- Name: queue_members queue_members_uniqueid_key; Type: CONSTRAINT; Schema: v1; Owner: pbx_ra_user
--

ALTER TABLE ONLY v1.queue_members
    ADD CONSTRAINT queue_members_uniqueid_key UNIQUE (uniqueid);


--
-- Name: queues queues_pkey; Type: CONSTRAINT; Schema: v1; Owner: pbx_ra_user
--

ALTER TABLE ONLY v1.queues
    ADD CONSTRAINT queues_pkey PRIMARY KEY (name);


--
-- PostgreSQL database dump complete
--

