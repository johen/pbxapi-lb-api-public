--
-- PostgreSQL database dump
--

-- Dumped from database version 12.6 (Debian 12.6-1.pgdg100+1)
-- Dumped by pg_dump version 12.6 (Ubuntu 12.6-1.pgdg20.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: pbxapi; Type: DATABASE; Schema: -; Owner: pbxapi_rw
--

CREATE DATABASE pbxapi WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.utf8' LC_CTYPE = 'en_US.utf8';


ALTER DATABASE pbxapi OWNER TO pbxapi_rw;

\connect pbxapi

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: cluster_api_security; Type: SCHEMA; Schema: -; Owner: pbxapi_rw
--

CREATE SCHEMA cluster_api_security;


ALTER SCHEMA cluster_api_security OWNER TO pbxapi_rw;

--
-- Name: cluster_events3; Type: SCHEMA; Schema: -; Owner: pbxapi_rw
--

CREATE SCHEMA cluster_events3;


ALTER SCHEMA cluster_events3 OWNER TO pbxapi_rw;

--
-- Name: data_change_event; Type: TYPE; Schema: cluster_events3; Owner: postgres
--

CREATE TYPE cluster_events3.data_change_event AS ENUM (
    'inserted',
    'updated',
    'deleted'
);


ALTER TYPE cluster_events3.data_change_event OWNER TO pbxapi_rw;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: group_members; Type: TABLE; Schema: cluster_api_security; Owner: pbxapi_rw
--

CREATE TABLE cluster_api_security.group_members (
    groupname character varying(64) NOT NULL,
    username character varying(64) NOT NULL,
    description character varying(256)
);


ALTER TABLE cluster_api_security.group_members OWNER TO pbxapi_rw;

--
-- Name: groups; Type: TABLE; Schema: cluster_api_security; Owner: pbxapi_rw
--

CREATE TABLE cluster_api_security.groups (
    groupname character varying(64) NOT NULL,
    description character varying(256)
);


ALTER TABLE cluster_api_security.groups OWNER TO pbxapi_rw;

--
-- Name: users; Type: TABLE; Schema: cluster_api_security; Owner: pbxapi_rw
--

CREATE TABLE cluster_api_security.users (
    username character varying(64) NOT NULL,
    token character varying(256) NOT NULL,
    description character varying(256)
);


ALTER TABLE cluster_api_security.users OWNER TO pbxapi_rw;

--
-- Name: agentconnections; Type: TABLE; Schema: cluster_events3; Owner: pbxapi_rw
--

CREATE TABLE cluster_events3.agentconnections (
    pbxapi_node character varying(64) NOT NULL,
    event character varying(64) NOT NULL,
    channel character varying(64) NOT NULL,
    uniqueid character varying(64) NOT NULL,
    "timestamp" timestamp without time zone NOT NULL,
    channelstate integer,
    channelstatedesc character varying(64),
    calleridnum character varying(64),
    calleridname character varying(64),
    connectedlinenum character varying(64),
    connectedlinename character varying(64),
    language character varying(2),
    accountcode character varying(64),
    context character varying(64),
    exten character varying(64),
    priority character varying(64),
    linkedid character varying(64),
    destchannel character varying(64),
    destuniqueid character varying(64),
    destchannelstate integer,
    destchannelstatedesc character varying(64),
    destcalleridnum character varying(64),
    destcalleridname character varying(64),
    destconnectedlinenum character varying(64),
    destconnectedlinename character varying(64),
    destlanguage character varying(2),
    destaccountcode character varying(64),
    destcontext character varying(64),
    destexten character varying(64),
    destpriority character varying(64),
    destlinkedid character varying(64),
    queue character varying(64) NOT NULL,
    membername character varying(64),
    interface character varying(64) NOT NULL,
    ringtime integer,
    holdtime integer,
    talktime integer,
    reason character varying(64),
    correlation_id uuid NOT NULL
);


ALTER TABLE cluster_events3.agentconnections OWNER TO pbxapi_rw;

--
-- Name: bridgemembership; Type: TABLE; Schema: cluster_events3; Owner: pbxapi_rw
--

CREATE TABLE cluster_events3.bridgemembership (
    pbxapi_node character varying(64) NOT NULL,
    event character varying(64) NOT NULL,
    channel character varying(64) NOT NULL,
    uniqueid character varying(64) NOT NULL,
    "timestamp" timestamp without time zone NOT NULL,
    channelstate integer,
    channelstatedesc character varying(64),
    calleridnum character varying(64),
    calleridname character varying(64),
    connectedlinenum character varying(64),
    connectedlinename character varying(64),
    language character varying(2),
    accountcode character varying(64),
    context character varying(64),
    exten character varying(64),
    priority character varying(64),
    linkedid character varying(64),
    bridgeuniqueid uuid NOT NULL,
    bridgetype character varying(64) NOT NULL,
    bridgetechnology character varying(64) NOT NULL,
    bridgecreator character varying(64),
    bridgename character varying(64),
    bridgenumchannels integer NOT NULL,
    bridgevideosourcemode character varying(64),
    bridgevideosource character varying(64),
    swapuniqueid character varying(64),
    correlation_id uuid NOT NULL
);


ALTER TABLE cluster_events3.bridgemembership OWNER TO pbxapi_rw;

--
-- Name: bridges; Type: TABLE; Schema: cluster_events3; Owner: pbxapi_rw
--

CREATE TABLE cluster_events3.bridges (
    pbxapi_node character varying(64) NOT NULL,
    event character varying(64) NOT NULL,
    "timestamp" timestamp without time zone NOT NULL,
    bridgeuniqueid uuid NOT NULL,
    bridgetype character varying(64) NOT NULL,
    bridgetechnology character varying(64) NOT NULL,
    bridgecreator character varying(64),
    bridgename character varying(64),
    bridgenumchannels integer NOT NULL,
    bridgevideosourcemode character varying(64),
    bridgevideosource character varying(64),
    correlation_id uuid NOT NULL
);


ALTER TABLE cluster_events3.bridges OWNER TO pbxapi_rw;

--
-- Name: channels; Type: TABLE; Schema: cluster_events3; Owner: pbxapi_rw
--

CREATE TABLE cluster_events3.channels (
    pbxapi_node character varying(64) NOT NULL,
    event character varying(64) NOT NULL,
    channel character varying(64) NOT NULL,
    uniqueid character varying(64) NOT NULL,
    "timestamp" timestamp without time zone NOT NULL,
    channelstate integer,
    channelstatedesc character varying(64),
    calleridnum character varying(64),
    calleridname character varying(64),
    connectedlinenum character varying(64),
    connectedlinename character varying(64),
    language character varying(2),
    accountcode character varying(64),
    context character varying(64),
    exten character varying(64),
    priority character varying(64),
    linkedid character varying(64),
    cause integer,
    cause_txt character varying(64),
    correlation_id uuid NOT NULL
);


ALTER TABLE cluster_events3.channels OWNER TO pbxapi_rw;

--
-- Name: qs_queueentries; Type: TABLE; Schema: cluster_events3; Owner: pbxapi_rw
--

CREATE TABLE cluster_events3.qs_queueentries (
    pbxapi_node character varying(64) NOT NULL,
    "timestamp" timestamp without time zone NOT NULL,
    event cluster_events3.data_change_event DEFAULT 'inserted'::cluster_events3.data_change_event NOT NULL,
    queue character varying(64) NOT NULL,
    "position" integer,
    channel character varying(64) NOT NULL,
    uniqueid character varying(64) NOT NULL,
    calleridnum character varying(64),
    calleridname character varying(64),
    connectedlinenum character varying(64),
    connectedlinename character varying(64),
    wait integer,
    priority integer,
    correlation_id uuid NOT NULL
);


ALTER TABLE cluster_events3.qs_queueentries OWNER TO pbxapi_rw;

--
-- Name: qs_queuemembers; Type: TABLE; Schema: cluster_events3; Owner: pbxapi_rw
--

CREATE TABLE cluster_events3.qs_queuemembers (
    pbxapi_node character varying(64) NOT NULL,
    "timestamp" timestamp without time zone NOT NULL,
    event cluster_events3.data_change_event DEFAULT 'inserted'::cluster_events3.data_change_event NOT NULL,
    queue character varying(64) NOT NULL,
    name character varying(64),
    location character varying(64) NOT NULL,
    stateinterface character varying(64),
    membership character varying(64),
    penalty integer,
    callstaken integer,
    lastcall timestamp without time zone,
    lastpause timestamp without time zone,
    incall integer NOT NULL,
    status integer NOT NULL,
    paused integer NOT NULL,
    pausedreason character varying(64),
    wrapuptime integer,
    correlation_id uuid NOT NULL
);


ALTER TABLE cluster_events3.qs_queuemembers OWNER TO pbxapi_rw;

--
-- Name: qs_queueparams; Type: TABLE; Schema: cluster_events3; Owner: pbxapi_rw
--

CREATE TABLE cluster_events3.qs_queueparams (
    pbxapi_node character varying(64) NOT NULL,
    "timestamp" timestamp without time zone NOT NULL,
    event cluster_events3.data_change_event DEFAULT 'inserted'::cluster_events3.data_change_event NOT NULL,
    queue character varying(64) NOT NULL,
    max integer,
    strategy character varying(64),
    calls integer,
    holdtime integer,
    talktime integer,
    completed integer,
    abandoned integer,
    servicelevel integer,
    servicelevelperf double precision,
    servicelevelperf2 double precision,
    weight integer,
    correlation_id uuid NOT NULL
);


ALTER TABLE cluster_events3.qs_queueparams OWNER TO pbxapi_rw;

--
-- Name: queuecallers; Type: TABLE; Schema: cluster_events3; Owner: pbxapi_rw
--

CREATE TABLE cluster_events3.queuecallers (
    pbxapi_node character varying(64) NOT NULL,
    event character varying(64) NOT NULL,
    channel character varying(64) NOT NULL,
    uniqueid character varying(64) NOT NULL,
    "timestamp" timestamp without time zone NOT NULL,
    channelstate integer,
    channelstatedesc character varying(64),
    calleridnum character varying(64),
    calleridname character varying(64),
    connectedlinenum character varying(64),
    connectedlinename character varying(64),
    language character varying(2),
    accountcode character varying(64),
    context character varying(64),
    exten character varying(64),
    priority character varying(64),
    linkedid character varying(64),
    queue character varying(64) NOT NULL,
    "position" integer,
    count integer,
    correlation_id uuid NOT NULL
);


ALTER TABLE cluster_events3.queuecallers OWNER TO pbxapi_rw;

--
-- Name: queuememberstatus; Type: TABLE; Schema: cluster_events3; Owner: pbxapi_rw
--

CREATE TABLE cluster_events3.queuememberstatus (
    pbxapi_node character varying(64) NOT NULL,
    event character varying(64) NOT NULL,
    "timestamp" timestamp without time zone NOT NULL,
    queue character varying(64) NOT NULL,
    membername character varying(64),
    interface character varying(64) NOT NULL,
    stateinterface character varying(64),
    membership character varying(64),
    penalty integer,
    callstaken integer,
    lastcall timestamp without time zone,
    lastpause timestamp without time zone,
    incall integer NOT NULL,
    status integer NOT NULL,
    paused integer NOT NULL,
    pausedreason character varying(64),
    ringinuse integer NOT NULL,
    wrapuptime integer,
    correlation_id uuid NOT NULL
);


ALTER TABLE cluster_events3.queuememberstatus OWNER TO pbxapi_rw;

--
-- Name: variables; Type: TABLE; Schema: cluster_events3; Owner: pbxapi_rw
--

CREATE TABLE cluster_events3.variables (
    pbxapi_node character varying(64) NOT NULL,
    event character varying(64) NOT NULL,
    channel character varying(64) NOT NULL,
    uniqueid character varying(64) NOT NULL,
    "timestamp" timestamp without time zone NOT NULL,
    channelstate integer,
    channelstatedesc character varying(64),
    calleridnum character varying(64),
    calleridname character varying(64),
    connectedlinenum character varying(64),
    connectedlinename character varying(64),
    language character varying(2),
    accountcode character varying(64),
    context character varying(64),
    exten character varying(64),
    priority character varying(64),
    linkedid character varying(64),
    variable character varying(64) NOT NULL,
    value character varying(256),
    correlation_id uuid NOT NULL
);


ALTER TABLE cluster_events3.variables OWNER TO pbxapi_rw;

--
-- Name: group_members group_members_pk; Type: CONSTRAINT; Schema: cluster_api_security; Owner: pbxapi_rw
--

ALTER TABLE ONLY cluster_api_security.group_members
    ADD CONSTRAINT group_members_pk PRIMARY KEY (groupname, username);


--
-- Name: groups groups_pk; Type: CONSTRAINT; Schema: cluster_api_security; Owner: pbxapi_rw
--

ALTER TABLE ONLY cluster_api_security.groups
    ADD CONSTRAINT groups_pk PRIMARY KEY (groupname);


--
-- Name: users users_pk; Type: CONSTRAINT; Schema: cluster_api_security; Owner: pbxapi_rw
--

ALTER TABLE ONLY cluster_api_security.users
    ADD CONSTRAINT users_pk PRIMARY KEY (username);


--
-- Name: agentconnections agentconnections_pk; Type: CONSTRAINT; Schema: cluster_events3; Owner: pbxapi_rw
--

ALTER TABLE ONLY cluster_events3.agentconnections
    ADD CONSTRAINT agentconnections_pk PRIMARY KEY (pbxapi_node, channel, uniqueid, queue, interface);


--
-- Name: bridgemembership bridgemembership_pk; Type: CONSTRAINT; Schema: cluster_events3; Owner: pbxapi_rw
--

ALTER TABLE ONLY cluster_events3.bridgemembership
    ADD CONSTRAINT bridgemembership_pk PRIMARY KEY (pbxapi_node, channel, uniqueid, bridgeuniqueid);


--
-- Name: bridges bridges_pk; Type: CONSTRAINT; Schema: cluster_events3; Owner: pbxapi_rw
--

ALTER TABLE ONLY cluster_events3.bridges
    ADD CONSTRAINT bridges_pk PRIMARY KEY (pbxapi_node, bridgeuniqueid);


--
-- Name: channels channels_pk; Type: CONSTRAINT; Schema: cluster_events3; Owner: pbxapi_rw
--

ALTER TABLE ONLY cluster_events3.channels
    ADD CONSTRAINT channels_pk PRIMARY KEY (pbxapi_node, channel, uniqueid);


--
-- Name: qs_queueentries qs_queueentries_pk; Type: CONSTRAINT; Schema: cluster_events3; Owner: pbxapi_rw
--

ALTER TABLE ONLY cluster_events3.qs_queueentries
    ADD CONSTRAINT qs_queueentries_pk PRIMARY KEY (pbxapi_node, channel, uniqueid, queue);


--
-- Name: qs_queuemembers qs_queuemembers_pkey; Type: CONSTRAINT; Schema: cluster_events3; Owner: pbxapi_rw
--

ALTER TABLE ONLY cluster_events3.qs_queuemembers
    ADD CONSTRAINT qs_queuemembers_pkey PRIMARY KEY (pbxapi_node, queue, location);


--
-- Name: qs_queueparams qs_queueparams_pk; Type: CONSTRAINT; Schema: cluster_events3; Owner: pbxapi_rw
--

ALTER TABLE ONLY cluster_events3.qs_queueparams
    ADD CONSTRAINT qs_queueparams_pk PRIMARY KEY (pbxapi_node, queue);


--
-- Name: queuecallers queuecallers_pk; Type: CONSTRAINT; Schema: cluster_events3; Owner: pbxapi_rw
--

ALTER TABLE ONLY cluster_events3.queuecallers
    ADD CONSTRAINT queuecallers_pk PRIMARY KEY (pbxapi_node, channel, uniqueid, queue);


--
-- Name: queuememberstatus queuememberstatus_pk; Type: CONSTRAINT; Schema: cluster_events3; Owner: pbxapi_rw
--

ALTER TABLE ONLY cluster_events3.queuememberstatus
    ADD CONSTRAINT queuememberstatus_pk PRIMARY KEY (pbxapi_node, queue, interface);


--
-- Name: variables variables_pk; Type: CONSTRAINT; Schema: cluster_events3; Owner: pbxapi_rw
--

ALTER TABLE ONLY cluster_events3.variables
    ADD CONSTRAINT variables_pk PRIMARY KEY (pbxapi_node, channel, uniqueid, variable);


--
-- Name: groups_groupname_uindex; Type: INDEX; Schema: cluster_api_security; Owner: pbxapi_rw
--

CREATE UNIQUE INDEX groups_groupname_uindex ON cluster_api_security.groups USING btree (groupname);


--
-- Name: users_username_uindex; Type: INDEX; Schema: cluster_api_security; Owner: pbxapi_rw
--

CREATE UNIQUE INDEX users_username_uindex ON cluster_api_security.users USING btree (username);


--
-- Name: group_members group_members_groups_groupname_fk; Type: FK CONSTRAINT; Schema: cluster_api_security; Owner: pbxapi_rw
--

ALTER TABLE ONLY cluster_api_security.group_members
    ADD CONSTRAINT group_members_groups_groupname_fk FOREIGN KEY (groupname) REFERENCES cluster_api_security.groups(groupname) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: group_members group_members_users_username_fk; Type: FK CONSTRAINT; Schema: cluster_api_security; Owner: pbxapi_rw
--

ALTER TABLE ONLY cluster_api_security.group_members
    ADD CONSTRAINT group_members_users_username_fk FOREIGN KEY (username) REFERENCES cluster_api_security.users(username) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- PostgreSQL database dump complete
--

