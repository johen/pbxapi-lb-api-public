--
-- PostgreSQL database dump
--

-- Dumped from database version 12.5 (Debian 12.5-1.pgdg100+1)
-- Dumped by pg_dump version 12.5 (Ubuntu 12.5-1.pgdg20.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: kamailio; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE kamailio WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';


ALTER DATABASE kamailio OWNER TO postgres;

\connect kamailio

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: subscriber; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.subscriber (
    id integer NOT NULL,
    username character varying(64) DEFAULT ''::character varying NOT NULL,
    domain character varying(64) DEFAULT ''::character varying NOT NULL,
    password character varying(64) DEFAULT ''::character varying NOT NULL,
    ha1 character varying(128) DEFAULT ''::character varying NOT NULL,
    ha1b character varying(128) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE public.subscriber OWNER TO postgres;

--
-- Name: subscriber_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.subscriber_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.subscriber_id_seq OWNER TO postgres;

--
-- Name: subscriber_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.subscriber_id_seq OWNED BY public.subscriber.id;


--
-- Name: subscriber id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.subscriber ALTER COLUMN id SET DEFAULT nextval('public.subscriber_id_seq'::regclass);


--
-- Data for Name: subscriber; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.subscriber (id, username, domain, password, ha1, ha1b) FROM stdin;
1	websip321	ss5.company.ru	1QaZ2WsX3EdC	4b70df1a0f884354f3dd27f5b0f82579	3d668a80a81a4b8d1dff7f10c99bead4
2	327	ss5.company.ru	jjdfhbkljHBJ74838djsk	91a5cead3c98b3a2cf42ac6351dd5854	7080160cee43796e70fb45825d438785
3	328	ss5.company.ru	skskdjfnlk299d89fjksjk	d5f677853195a6c0a4ede3c44cec337f	5d869588724a76d92170a0c46687c922
4	330	ss5.company.ru	pxveOKcnA4tz	f678c11bd1ba6b5b8020a28fb6102d86	503c1152df7f835c0c6d53b8abaa3c5e
5	331	ss5.company.ru	rvkuV9OUMxpN	0885b35d0999787de6a8e3d546ed4a31	90f347775386333c983b588dde278441
6	332	ss5.company.ru	Qe6YLF09eVBS	2b8f14539b4f3c620146f31ab203d518	6275c4bb383b350294753e4dab074e48
7	333	ss5.company.ru	Va6AQ3TL1HSO	755aaa9bd79a1f902913800adda75b24	ae690b2f71dc80207486a7c6914cda43
8	334	ss5.company.ru	xkpj9eHmThNK	942402d7c915bbd0de13b1c7c02ceba9	32e6dbf264b835a104d91a949d9a687c
9	335	ss5.company.ru	P12P7vatNLrx	62dc2e7ae9c0ce6091c23d31ef33b677	7fb688a7c31f5b761d3eeb3b6a43a945
10	336	ss5.company.ru	2O87k217Vmq0	9b477e948be28d24acdc56cdba8fb954	1bc5dc0c59b868f5d7a9629434cccd06
11	337	ss5.company.ru	8yjfpln17iui	f5498715a45332c0c7ec53bf6b2f18c6	9c5d686b80d2e5276bf4885fddfe3237
14	338	ss5.company.ru	HPLvwKzddlia	f3cb7affd4e96900525ecb74dfa352be	31b99a9fa919fcc3a6770dabdba0fb12
15	339	ss5.company.ru	230hx2Xk559g	83e27085d1361f098107c3c5a721b3e9	0e5e10a033e37f0a86923909c77b6f26
17	340	ss5.company.ru	tSBLxOSX4V69	9d7a242570f15617a2de3883b387238b	c68e57afe85bde544735ea777c68edc8
18	341	ss5.company.ru	dZt8xWNxxeip	d732c358862ee2ee5c5b956b24825252	8cafcd81c253d349ae45b8290198cf5a
19	342	ss5.company.ru	Tz2sIyF85GQV	0c9ab958bfd188528aa4ba0002cc7f33	ba7c0fbec173a08ad139cc5d49443a31
20	343	ss5.company.ru	rM3IskwdFA1K	668718da30ec11c1a76f22de469c3670	da81b5533d76bffd288c0b2dfe36e93d
21	344	ss5.company.ru	9kp1qmGhAEPU	42bf573049b39ca799ee5e27df3e187b	e7668260ed17d1bee7797474e7b5ff5f
22	345	ss5.company.ru	0dc4J9lGx5iS	2c05c25b8bd38847f72c293f7becf14c	e30c0859337ebc263340a3299fbafc1d
23	346	ss5.company.ru	z6NO5AgaaR2B	6904c10c7d37a95dc0e3b9bf27d8dbde	e8955c2d34de19af98859b5352232b70
24	347	ss5.company.ru	aD1pRxcIpQ7H	57d5bc74dcca8db9768db59693689ec1	40b312eef7269efb731fbb4d6e058637
25	348	ss5.company.ru	SF9QGXdN28yt	e512d9a125ae9795f7416664d49fbf82	4e4497da3b9ddbff8af33003ea5a6ed7
26	349	ss5.company.ru	VWCnKAOszGaf	8000d80d784182f2f706ad89d6ced856	1daa696c8391693f8eba22c6ec7a5f33
27	350	ss5.company.ru	NQPxNOiQ2xye	4242c156e3efb56eddf3998146c8ea82	def0dc19dacb2e2129c3b70eff9c3e2e
28	351	ss5.company.ru	hI5F7gjFP6Pn	960f7606c7631a8e3b09fc6154677e5b	0bdc6869310a62ffc7d20907f9766d4d
29	352	ss5.company.ru	zKfPuc39yuhf	811a1d3a3e9f2a3e3ee99a38fcccefdb	7425e99d094fc6f02c1cd705d3d4ad15
30	353	ss5.company.ru	hxV3EzLuCa7f	d1fa125dffada69335397bed2d67c726	cb5cc6daeeb640ba6b4d2ac2968f526f
31	354	ss5.company.ru	n3KrrOLUN022	93a4505ed6571796f2fe9c7be9bc010e	d1146458f7da254624ff5996509c3bdb
32	355	ss5.company.ru	0CH92OwegkgB	343ebbf9e1a7730674f0892e4217ff85	bfe6929173e4d826725df2f1ea105d8d
33	356	ss5.company.ru	bIrTgedcyAZN	d14c2ceecd216a14172bec815c4bebce	07357ba10a8723489daf3c55ae3d6aae
34	357	ss5.company.ru	41hSNtBFilBS	808e40b0eea1103f1b62974e980dfa54	38ecbb74947533c26c2ead51f2f7bf34
35	358	ss5.company.ru	mLFYwCMYnUtp	e96154451f984ac086ff540926ad491c	f2f81526fabe4a2cf2a44d807bb7e48a
36	359	ss5.company.ru	KYRhSdSiQynm	2c5afb6abea84faf11c6ce54607a682b	c73c3a7e10f5639bb1e68000d948c14d
37	360	ss5.company.ru	nlMHJO5Nak7e	859b813eaeed423f0a34f1eedaf3eff7	4961ca5b8b86bfdf7bee769a75061653
38	361	ss5.company.ru	6QQJb1FKhohl	70b70a35e051f77b7f19eb5648924e4e	777511d6f4d9cf4e17606141d3c89d47
39	362	ss5.company.ru	C6PI0OI8HFIN	d8caf2e4172f0593b2d447912fec35ae	a25ab57a537cc3a743eba545da4d65c2
40	363	ss5.company.ru	boAGjqpccTXf	5e52931c6dcaee2db763074a78fc67e2	52036bc6fec7ef2b685a8a6d22547d0f
41	364	ss5.company.ru	rHjCXvUNGdG6	2720be20f8b660813a9526da82bcfef3	7edc821dee548f6e0557169aabb1e4e4
42	365	ss5.company.ru	y3zUfLqQqGdN	6b06bc81c5831a899b385cbbf2e5709b	efd14f44e7184afcb4b87b9322d7430e
43	366	ss5.company.ru	Lh8nIOYcklmW	2790d635d7483cbc9999ec315d56368c	f278332075683399ec97186b50c679f4
44	367	ss5.company.ru	P4By77ivx6Th	61014cca240aab02557c2390b4177cef	45c8e8808bcaffa79f9743308aeaba0e
45	368	ss5.company.ru	PQuzMSmcHCXF	746bd0e3850f8dc24dc2c9cd30433021	e15814dba9b26c4787afa7d9779c6ce4
46	369	ss5.company.ru	DTOiOLxkaOvM	75b252be7d282c717ee15082513f277f	9c88fab8e1f36743568cc18a425d284b
47	370	ss5.company.ru	7yq6cTiiifiI	69ce4f5d94fa6ff55f52c14dea8613e4	7ec822b4b63a0de15fb42887283f0867
48	371	ss5.company.ru	Gx5qhdxFT2ga	cc90a9c3ef82b9284441b497e70beecf	0ad2645ea5ade42e077e41d754ceac82
49	372	ss5.company.ru	cF07XhHVNYA8	f1d8f18a599dc6c28040e5a15edfe205	a8aa857a142c5d454ae88f674d08969c
50	373	ss5.company.ru	Bxqtpuk9iWM9	65b44b4d3864c62dad3984962563f79c	34b0b45fcc4b559a60c316014586deba
51	374	ss5.company.ru	GfZeA5gEmTlw	0dfc10f44d81d80bb2d554e2125013fa	c605f1b5049f55450b2d38d27332baaf
52	375	ss5.company.ru	mKnzLEnfx8lj	7e2c2acd74f074d33d33644f3d98145d	fa5dc72cad067caed311e817a7b994bf
53	376	ss5.company.ru	Zr1PzjJv7VHP	f29f866b75d6c59e1989be1f5c21d6b3	f3cb4f916374f4bd07edd8ff8bc44398
54	377	ss5.company.ru	oiYbOufX7tJv	c87a61e88c8d783129c64088432e341b	6f88c3daa7f99f2b18522c71d757b5e0
55	378	ss5.company.ru	OggutEQdJOGa	b81cbae1d1d9f70a599de49f3a6f426c	f5fec498cf3733e1ae7f741de10eb35a
56	379	ss5.company.ru	SzozPqceKtZW	ad04b4a5547815c427a78c50f3cb1697	658de941b114e18bb448d6abf851606f
57	380	ss5.company.ru	QUeNPWz0ZnBb	af6fca55e5ca5a3d840c1d4490f52c8a	7ef265473bbf2b33f2cd90e77b558388
58	381	ss5.company.ru	mV34xe1AqfVx	90a931cc29d2fdfae2f4d8fc3bd0953c	2687472b75f3e424523de773a604aed1
59	382	ss5.company.ru	iiXqINQs8XqT	1ad7e1543035871e5776e13c45622dde	cb71812a5f77edf4eb45bb04018546ce
60	383	ss5.company.ru	mWeBtWG0W8nO	ff251bfd094b72ba8b5b781c29f19af2	98923aed878e6b1f9779495ebaf847c1
61	384	ss5.company.ru	qI6MDSO8IWBn	35bae46017794182e437e0d01ed39388	cfb2d60aea5ace3bc6e4d34cf8912cd0
62	385	ss5.company.ru	1B3M7BgtkkSl	172cd29329abd567ecd0db7ddf9046ab	4cbe0a1803aef0fe05893fd32b294c91
63	386	ss5.company.ru	zLtuHIOmshok	cafd65fbcf83d418bb4d4c2cec5917ec	65f5663da2b4b6fad57cd820d18142ec
64	387	ss5.company.ru	6TEzXrasFsty	8f527103868595489f057b7475023661	1998bbabbd486c3c2951fce948e484c1
65	388	ss5.company.ru	vTVJZXbV7vTC	0d324ca621b9ab3d434532d391373ae4	677167782aa4cab17f07c2ef1116fca1
66	389	ss5.company.ru	bVL87EZIvWpL	50d1db95ed000122fd6f9c15e3f2732c	d3ad47f2452620007d9fcbe128aea755
\.


--
-- Name: subscriber_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.subscriber_id_seq', 66, true);


--
-- Name: subscriber subscriber_account_idx; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.subscriber
    ADD CONSTRAINT subscriber_account_idx UNIQUE (username, domain);


--
-- Name: subscriber subscriber_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.subscriber
    ADD CONSTRAINT subscriber_pkey PRIMARY KEY (id);


--
-- Name: subscriber_username_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX subscriber_username_idx ON public.subscriber USING btree (username);


--
-- Name: TABLE subscriber; Type: ACL; Schema: public; Owner: postgres
--

GRANT USAGE on schema public to kamailio_api;
GRANT SELECT,UPDATE ON TABLE public.subscriber TO kamailio_api;


--
-- Name: SEQUENCE subscriber_id_seq; Type: ACL; Schema: public; Owner: postgres
--

-- GRANT SELECT,USAGE ON SEQUENCE public.subscriber_id_seq TO kamailio;


--
-- PostgreSQL database dump complete
--

