\connect pbxapi

COPY cluster_api_security.groups (groupname, description) FROM stdin;
crm	\N
adm	\N
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: cluster_api_security; Owner: pbxapi_rw
--

COPY cluster_api_security.users (username, token, description) FROM stdin;
crm	1234567890	\N
\.


--
-- Data for Name: group_members; Type: TABLE DATA; Schema: cluster_api_security; Owner: pbxapi_rw
--

COPY cluster_api_security.group_members (groupname, username, description) FROM stdin;
crm	crm	\N
\.


