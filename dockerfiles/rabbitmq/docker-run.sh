#!/bin/bash

docker_tag=johen/pbxapi-ra-api_rmq
docker_container=pbxapi-ra-api_rmq

docker build -t $docker_tag -f ./Dockerfile . && \
#docker build -t $docker_tag -f ./dockerfiles/Dockerfile . && \
docker run -i --rm \
-h listener \
-p 15672:15672 \
-p 5672:5672 \
--name $docker_container \
$docker_tag