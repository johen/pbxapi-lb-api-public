import traceback
from typing import Dict, List

from panoramisk import Manager
from pydantic import BaseModel

from core.logger import logger_panoramisk, logger
import random


class AMI:
    """
    Класс для нескольких подключений через panoramisk сразу к различным астерам
    """

    # TODO: Необходимо, чтобы в ошибках писался какой именно менеджер генерит ошибку.
    #  А то сейчас не понятно: "Not able to connect" - куда невозможен коннект?

    def __init__(self):
        self._managers: List[Manager] = []
        self._config: Dict[str, dict] = {}

    def config(self, config: dict):
        self._config = config

    def initialize(self):
        if self._config == {}:
            raise ValueError('Config dictionary is empty')

        self._managers = []
        for node, cfg in self._config.items():
            manager = Manager(host=cfg['host'],
                              port=cfg['port'],
                              username=cfg['username'],
                              secret=cfg['secret'],
                              ping_delay=cfg['ping_delay'],
                              ping_interval=cfg['ping_interval'],
                              reconnect_timeout=cfg['reconnect_timeout'],
                              log=logger_panoramisk,
                              forgetable_actions=('ping', 'login', 'originate'),
                              events='off',
                              name=node
                              )

            # TODO: Manager не реагирует на внезапную недоступность ноды астера. Пример: врубить ufw.
            #  При включении менеджер перестает слать пинг и не рейзит on_disconnect, поэтому апи считает, что нода
            #  доступна. Такое ощущение, что где-то не обрабатывается такой отвал на уровне asyncio в factory
            #  AMIProtocol в библиотеке.

            # def on_connect(mngr: Manager):
            #     print('CONNECTED')

            # def on_disconnect(mngr: Manager, exc: Exception):
            #     print('DISCONNECTED')

            # manager.on_connect = on_connect
            # manager.on_disconnect = on_disconnect

            manager.connect()

            self._managers.append(manager)

    async def send_action(self, action: BaseModel, as_list: bool = None):
        """
        Отправка экшена на рандомную живую ноду
        :param as_list: ответ multiline (несколько ивентов) или возврат сразу после первого (для Originate указывать
         as_list=False)
        :param action: данные экшена в формате модели pydantic
        :return:
        """
        connected_nodes = []
        for node in self._managers:
            if node._connected:
                connected_nodes.append(node)

        logger.debug('Count of live asterisk nodes == ' + str(len(connected_nodes)))

        if not connected_nodes:
            logger.warning('There is no live asterisk nodes')
            raise AMI.ErrorNoLiveNode

        active_manager = random.choice(connected_nodes)
        logger.debug('Active manager choiced randomly: ' + active_manager.config['name'])

        try:
            future_obj = await active_manager.send_action(action.dict(exclude_none=True, by_alias=True),
                                                          as_list=as_list)
        except Exception as err:
            logger.critical('ABNORMAL EXCEPTION RAISED (' + str(repr(err)) + ') while sending action: ' +
                            str(action.dict(exclude_none=True, by_alias=True)))
            logger.debug('Traceback:\n' + traceback.format_exc())
            raise
        logger.debug('Action sent')

        return future_obj

    # TODO: panoramisk скорее всего поставит экшен в очередь, если нет коннекта. Нужно, чтобы он не пытался
    #  отправить экшен потом при возобновлении соединения. Скорее всего нужно смотреть в forgetable_actions.
    #  Еще лучше - если экшен невозможно отправить на конкретную ноду, то отправлять ее на другую доступную.
    #  Это поведение должно быть настраиваемо через опции к функциям отправки

    def close(self):
        for manager in self._managers:
            manager.close()

    class ErrorNoLiveNode(Exception):
        pass


ami = AMI()
