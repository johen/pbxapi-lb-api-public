from typing import Optional, List

from pydantic import BaseModel, Field, validator

from ami.manager import AMI, ami
from core.logger import logger


class OriginateActionData(BaseModel):
    action = Field('Originate', alias='Action')
    actionid: Optional[str] = Field(None, alias='ActionID')
    # Унификация: чтобы везде не указывать SIP/. Плюс см. валидатор ниже, который трансформирует channel
    channel: str = Field(..., regex='^(?!SIP/).+', alias='Channel')
    exten: str = Field(..., alias='Exten')
    context: str = Field(..., alias='Context')
    priority: str = Field(..., alias='Priority')
    application: Optional[str] = Field(None, alias='Application')
    data: Optional[str] = Field(None, alias='Data')
    timeout: Optional[int] = Field(None, alias='Timeout')
    callerid: Optional[str] = Field(None, alias='CallerID')
    variable: Optional[List[str]] = Field(None, alias='Variable')
    account: Optional[str] = Field(None, alias='Account')
    earlymedia: Optional[bool] = Field(None, alias='EarlyMedia')
    async_flag: Optional[bool] = Field(True, alias='Async')
    codecs: Optional[List[str]] = Field(None, alias='Codecs')
    channelid: Optional[str] = Field(None, alias='ChannelId')
    otherchannelid: Optional[str] = Field(None, alias='OtherChannelId')

    @validator('channel')
    def update_with_technology(cls, v):
        return 'SIP/' + v

    class Config:
        allow_population_by_field_name = True


class OriginateFromCRMActionData(OriginateActionData):
    context = 'fromCRM'
    priority = '1'
    callerid = 'From CRM <PBX>'


def action_originate(action: OriginateActionData):
    try:
        return ami.send_action(action, as_list=False)
    except AMI.ErrorNoLiveNode:
        logger.error('No live connections to asterisk nodes. All are down! '
                     'Action: ' + str(action.dict()))
        return None
    except Exception:
        raise
