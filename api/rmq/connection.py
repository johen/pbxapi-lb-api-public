from celery import Celery

from core.config import config
from core.logger import logger


rmq_publisher = Celery('rmq_publisher', log=logger)
rmq_publisher.conf.update(config['rmq_publisher'])
rmq_publisher.conf.task_routes = config['task_routes']['tasks']
