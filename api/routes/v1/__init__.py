from fastapi import FastAPI
import api.openapi_data as openapi_data


app = FastAPI(
    title=openapi_data.title,
    description=openapi_data.description,
    version=openapi_data.version,
    openapi_tags=openapi_data.tags,
)
