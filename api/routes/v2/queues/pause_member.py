from datetime import datetime
from typing import Optional

from fastapi import APIRouter
from pydantic import BaseModel, Field
from pydantic.fields import Undefined
from sqlalchemy import update, and_
from starlette import status
from starlette.background import BackgroundTask, BackgroundTasks
from starlette.exceptions import HTTPException
from starlette.responses import Response

from core.config import config
from core.logger import logger
from database.connection import db_ara
from database.tables_ara import t_queue_members
from routes.v2.common_func import member_event_queue_log, request_queueshow_update
from routes.v2.common_models import QueueMember

router = APIRouter()


class QueueMemberData(BaseModel):
    interface: int = Field(..., ge=100, le=999,
                           example=311,
                           title='Внутренний номер оператора',
                           alias='dst')
    queue_name: str = Field(..., min_length=1, max_length=80,
                            example='SPB_CallCenter',
                            title='Наименование очереди',
                            alias='queue')
    paused: bool = Field(...,
                         example=False,
                         title='Постановка на паузу')
    reason: Optional[str] = Field(Undefined,
                                  example='dialog_opened',
                                  title='Причина постановки или снятия с паузы')


@router.post('/pause', status_code=status.HTTP_204_NO_CONTENT,
             summary='Пауза',
             description='Постановка или снятие с паузы',
             responses={
                 204: {'description': 'Пауза установлена или снята'},
                 400: {
                     'description': 'Указанный dst отсутствует в указанной очереди',
                     'content': {
                         'application/json': {
                             'example': {
                                 'detail': 'Dst ' +
                                           config['channel_driver'] + '/311 out of queue SPB_CallCenter'},
                         }
                     }
                 }
             },
             tags=['queues', 'crm'])
async def pause(queue_member: QueueMemberData):
    qm_for_db = QueueMember(**queue_member.dict())

    stmt = update(t_queue_members). \
        where(and_(t_queue_members.c.interface == qm_for_db.interface,
                   t_queue_members.c.queue_name == qm_for_db.queue_name,
                   t_queue_members.c.event == 'added')). \
        values({'paused': qm_for_db.paused,
                'correlation_id': qm_for_db.correlation_id,
                'nodename': qm_for_db.nodename}). \
        returning(t_queue_members.c.uniqueid, t_queue_members.c.membername)

    try:
        result = await db_ara.conn.fetch_one(stmt)
        time = datetime.now()
    except Exception:
        raise

    if not result:
        raise HTTPException(status.HTTP_400_BAD_REQUEST,
                            detail='Dst ' + str(qm_for_db.interface) +
                                   ' out of queue ' + str(qm_for_db.queue_name))
    else:
        logger.debug('queue_members record with uniqueid ' + str(result['uniqueid']) +
                     ' pause set to ' + str(qm_for_db.paused))

    background_tasks = BackgroundTasks(
        [
            BackgroundTask(member_event_queue_log,
                           time=time,
                           event='PAUSE' if qm_for_db.paused else 'UNPAUSE',
                           agent=result['membername'],
                           queuename=qm_for_db.queue_name,
                           data1=queue_member.reason or ''),
            BackgroundTask(request_queueshow_update,
                           queuename=qm_for_db.queue_name),
        ]
    )

    return Response(status_code=status.HTTP_204_NO_CONTENT, background=background_tasks)
