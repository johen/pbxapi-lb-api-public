from fastapi import APIRouter
from .add_member import router as add_member_router
from .remove_member import router as remove_member_router
from .pause_member import router as pause_member_router


router = APIRouter()

router.include_router(add_member_router)
router.include_router(remove_member_router)
router.include_router(pause_member_router)
