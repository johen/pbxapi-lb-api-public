from datetime import datetime
from typing import Optional

from fastapi import APIRouter
from pydantic import BaseModel, Field
from pydantic.fields import Undefined
from sqlalchemy import update, and_, select
from sqlalchemy.dialects.postgresql import insert
from starlette import status
from starlette.background import BackgroundTask, BackgroundTasks
from starlette.exceptions import HTTPException
from starlette.responses import Response

from core.config import config
from core.logger import logger
from database.connection import db_ara
from database.tables_ara import t_queue_members
from routes.v2.common_func import member_event_queue_log, request_queueshow_update
from routes.v2.common_models import QueueMember

router = APIRouter()


class QueueMemberData(BaseModel):
    interface: int = Field(..., ge=100, le=999,
                           example=311,
                           title='Внутренний номер оператора. Существование не проверяется.',
                           alias='dst')
    queue_name: str = Field(..., min_length=1, max_length=80,
                            example='SPB_CallCenter',
                            title='Наименование очереди. Существование очереди не проверяется.',
                            alias='queue')
    membername: Optional[int] = Field(Undefined, gt=1,
                                      example=99999,
                                      title='Идентификатор юзера в CRM. Наличие в CRM не проверяется.',
                                      alias='user_id')
    penalty: Optional[int] = Field(0, ge=0,
                                   example=0,
                                   title='Пенальти члена очереди. Чем выше значение тем меньше приоритет того, что '
                                         'звонок попадет на этого члена очереди. А точнее: звонок прилетит только '
                                         'тогда, когда все члены очереди с более низким пенальти уже разговаривают.', )
    paused: Optional[bool] = Field(False,
                                   example=False,
                                   title='Установка на паузу члена очереди сразу после добавления?')


@router.post('/add', status_code=status.HTTP_204_NO_CONTENT,
             summary='Добавление члена в очередь',
             description='Добавление члена в очередь. Проверка на его существование не выполняется. '
                         'Отказ, если уже в очереди.',
             responses={
                 204: {'description': 'Член добавлен в очередь'},
                 400: {
                     'description': 'Dst уже в очереди',
                     'content': {
                         'application/json': {
                             'example': {
                                 'detail': 'Dst ' +
                                           config['channel_driver'] + '/311 already in queue SPB_CallCenter'},
                         }
                     }
                 }
             },
             tags=['queues', 'crm'])
async def add(queue_member: QueueMemberData):
    qm_for_db = QueueMember(**queue_member.dict())

    # Наиболее частый запрос: обновление инфы о члене в очереди. Запись в бд должна быть с event = deleted
    stmt_update = update(t_queue_members). \
        where(and_(t_queue_members.c.queue_name == qm_for_db.queue_name,
                   t_queue_members.c.interface == qm_for_db.interface,
                   t_queue_members.c.event == 'deleted')). \
        values(qm_for_db.dict()). \
        returning(t_queue_members.c.uniqueid)

    # Если предыдущий запрос не вернул uniqueid (вернул None), то делаем селект для проверки на наличие вообще записи.
    # Запрос должен вернуть результат с event == deleted. Если это так, то возвращаем 400 "Член уже в очереди"
    stmt_select = select([t_queue_members.c.uniqueid, t_queue_members.c.event]). \
        where(and_(t_queue_members.c.queue_name == qm_for_db.queue_name,
                   t_queue_members.c.interface == qm_for_db.interface))

    # Если Select не вернул результат, значит записи вообще нет, поэтому исполняем insert.
    stmt_insert = insert(t_queue_members). \
        values(qm_for_db.dict()). \
        returning(t_queue_members.c.uniqueid)

    try:
        async with db_ara.conn.transaction():
            update_result = await db_ara.conn.execute(stmt_update)
            time = datetime.now()

            if not update_result:
                select_result = await db_ara.conn.fetch_one(stmt_select)

                if not select_result:
                    insert_result = await db_ara.conn.execute(stmt_insert)
                    time = datetime.now()
                    logger.debug('queue_members new record inserted with uniqueid ' + str(insert_result))

                elif select_result['event'] == 'added':
                    raise HTTPException(status.HTTP_400_BAD_REQUEST,
                                        detail='Dst ' + str(qm_for_db.interface) +
                                               ' already in queue ' + str(qm_for_db.queue_name))
                else:
                    logger.error('Something strange while processing member add to queue: event != added or removed')
                    raise Exception
            else:
                logger.debug('queue_members record with uniqueid ' + str(update_result) + ' updated')
    except Exception:
        raise

    background_tasks = BackgroundTasks(
        [
            BackgroundTask(member_event_queue_log,
                           time=time,
                           event='ADDMEMBER',
                           agent=qm_for_db.membername,
                           queuename=qm_for_db.queue_name,
                           data1='PAUSED' if qm_for_db.paused else ''),
            BackgroundTask(request_queueshow_update,
                           queuename=qm_for_db.queue_name),
        ]
    )

    return Response(status_code=status.HTTP_204_NO_CONTENT, background=background_tasks)
