import hashlib
import secrets
from typing import Optional

from fastapi import APIRouter, Path, Body, Security
from fastapi.openapi.models import APIKey
from pydantic import Field, BaseModel
from pydantic.fields import Undefined
from sqlalchemy import update
from starlette.exceptions import HTTPException
from starlette.responses import JSONResponse
from starlette.status import HTTP_400_BAD_REQUEST, HTTP_200_OK

from api.core.config import config
from core.security import token_auth
from database.connection import db_webrtc
from database.tables_kamailio import t_subscriber

router = APIRouter()


class DstPasswd(BaseModel):
    passwd: Optional[str] = Field(min_length=16, max_length=64,
                                  regex='^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])([^\s]+)$',
                                  description='Пароль должен в себя включать хотя бы одну цифру и '
                                              'букву в нижнем и верхнем регистрах. '
                                              'В пароле не допускается наличие пробела.',
                                  example='jskjdjhbHhd_d299djKJJHksjdnjkkkdj')


@router.put('/dst/{dst}/passwd',
            summary='Обновление пароля',
            description='Генерация или установка пароля для указанного dst. Если параметры не передаются, то пароль '
                        'генерируется и устанавливается самой API. Сгенерированный пароль возвращается в ответе '
                        'и может содержать цифры, буквы в обоих регистрах и специальные символы.',
            responses={
                200: {'model': DstPasswd, 'description': 'Пароль установлен/сгенерен удачно'},
                400: {
                    'description': 'Dst не найден',
                    'content': {
                        'application/json': {
                            'example': {'detail': 'Dst 311 does not exist'},
                        }
                    }
                }
            })
async def update_passwd(dst: int = Path(..., ge=100, le=999, description='dst в трехзначном формате', example=311),
                        item_passwd: Optional[DstPasswd] = Body(Undefined,
                                                                description='Если не передается, '
                                                                            'то пароль генерируется'),
                        api_key: APIKey = Security(dependency=token_auth, scopes=["crm"])):
    if not item_passwd:
        passwd = secrets.token_urlsafe(32)
    else:
        passwd = item_passwd.passwd

    domain = config['database_webrtc']['domain']

    ha1 = hashlib.md5((str(dst) +
                       ':' +
                       domain +
                       ':' +
                       passwd).encode('utf-8')).hexdigest()

    ha1b = hashlib.md5((str(dst) + '@' + domain +
                        ':' +
                        domain +
                        ':' +
                        passwd).encode('utf-8')).hexdigest()

    stmt = update(t_subscriber).\
        where(t_subscriber.c.username == str(dst)).\
        values(domain=domain, password=passwd, ha1=ha1, ha1b=ha1b).\
        returning(t_subscriber.c.id)

    try:
        result = await db_webrtc.conn.execute(stmt)
    except Exception:
        raise

    if result is None:
        raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail='Dst ' + str(dst) + ' does not exist')

    return JSONResponse(DstPasswd(passwd=passwd).dict(), status_code=HTTP_200_OK)
