from fastapi import APIRouter
from .passwd_update import router as passwd_update_router


router = APIRouter()

router.include_router(passwd_update_router)
