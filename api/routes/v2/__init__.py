from database.connection import DBConnectionError
from .crm import router as crm_router
from .webrtc import router as webrtc_router
from .queues import router as queues_router

from fastapi import FastAPI
import api.openapi_data as openapi_data
from fastapi.exceptions import ValidationError, RequestValidationError
from core.exception_handlers import abnormal_exception_handler, http422_error_handler, nolivenodes_error_handler, \
    httpexception_error_handler
from ami.manager import AMI
from starlette.exceptions import HTTPException as StarletteHTTPException

app = FastAPI(
    title=openapi_data.title,
    description=openapi_data.description,
    version=openapi_data.version,
    openapi_tags=openapi_data.tags,
)

app.add_exception_handler(RequestValidationError, http422_error_handler)
app.add_exception_handler(AMI.ErrorNoLiveNode, nolivenodes_error_handler)
app.add_exception_handler(ValidationError, abnormal_exception_handler)
app.add_exception_handler(StarletteHTTPException, httpexception_error_handler)
app.add_exception_handler(DBConnectionError, abnormal_exception_handler)
app.add_exception_handler(Exception, abnormal_exception_handler)

app.include_router(crm_router,
                   prefix='/crm',
                   tags=['crm'])
app.include_router(webrtc_router,
                   prefix='/webrtc',
                   tags=['webrtc'])
app.include_router(queues_router,
                   prefix='/queue',
                   tags=['queues'])
