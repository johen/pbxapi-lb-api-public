import re
from datetime import datetime, timedelta
from typing import Optional, List

from fastapi import APIRouter, Query
from pydantic import BaseModel, Field, validator
from pydantic.fields import Undefined
from sqlalchemy import func, and_, desc, select
from starlette import status

from core.config import config
from core.logger import logger
from database.connection import db
from database.tables_events import t_qs_queuemembers, t_qs_queueentries

router = APIRouter()


class MemberModel(BaseModel):
    location: str = Field(...,
                          example=311,
                          title='Внутренний номер оператора',
                          alias='dst')
    name: Optional[str] = Field(example=99999,
                                title='Текущий идентификатор юзера в CRM',
                                alias='user_id')
    paused: bool = Field(...,
                         example=True,
                         title='В данный момент на паузе')
    pausedreason: Optional[str] = Field(example='Resting',
                                        title='Причина паузы, если установлена')
    incall: bool = Field(example=False,
                         title='В данный момент в данной очереди на телефоне')
    lastcall: datetime = Field(example='2021-04-30T15:00:54',
                               title='Время последнего принятого звонка')
    status: int = Field(example=2,
                        title='Статус интерфейса члена очереди')

    @validator('location', pre=True)
    def cut_channel_driver(cls, v):
        return str(v).replace(config['channel_driver'] + '/', '')

    @validator('name', pre=True)
    def get_user_id(cls, v):
        m = re.match(r'^.+user_id:(\d+).*$', str(v))
        if m:
            return m.group(1)
        else:
            return None

    class Config:
        allow_population_by_field_name = True


class EntryModel(BaseModel):
    position: int = Field(example=1, title='Позиция звонка в очереди')
    wait: int = Field(example=30, title='Время ожидания в секундах')


class QueueModel(BaseModel):
    queue: str = Field(title='Наименование очереди', example='SPB_CallCenter')
    members: Optional[List[MemberModel]] = Field([], title='Данные членов очереди, если есть',
                                                 example=[
                                                     {
                                                         "dst": "304",
                                                         "user_id": None,
                                                         "paused": False,
                                                         "pausedreason": "",
                                                         "incall": False,
                                                         "lastcall": "1970-01-01T03:00:00",
                                                         "status": 1
                                                     },
                                                     {
                                                         "dst": "305",
                                                         "user_id": None,
                                                         "paused": False,
                                                         "pausedreason": "",
                                                         "incall": False,
                                                         "lastcall": "1970-01-01T03:00:00",
                                                         "status": 1
                                                     },
                                                     {
                                                         "dst": "311",
                                                         "user_id": None,
                                                         "paused": False,
                                                         "pausedreason": "",
                                                         "incall": False,
                                                         "lastcall": "1970-01-01T03:00:00",
                                                         "status": 6
                                                     }
                                                 ])
    entries: Optional[List[EntryModel]] = Field([], title='Данные ожидающих звонков в очереди',
                                                example=[
                                                    {
                                                        "position": 1,
                                                        "wait": 3
                                                    }
                                                ])


@router.get('/queue-member-status', status_code=status.HTTP_200_OK, response_model=List[QueueModel],
            summary='Состояние очередей',
            description='Запрос возвращает информацию о состоянии очередей. '
                        'Является интерпретацией периодического запроса queuestatus. '
                        'Данные считаются устаревшими, если их старость превышает ' +
                        str(config['queue_status_out_of_date']) +
                        ' секунд (т.е. периодический опрос queuestatus выполнен настолько давно). '
                        'В этом случае данные не возвращаются.</br></br> '
                        
                        '**Внимание! Операции над членами очередей (постановка, удаление, пауза и ее снятие), '
                        'выполняемые через эту API, применяются сразу, но в выводе данного метода видны максимум '
                        'через ' + str(config['queueshow_update_countdown']) +
                        ' секунд.**</br></br> '
                        
                        'Также стоит учитывать, что из-за ограничений архитектуры ARA, причина постановки '
                        'на паузу или причина снятия с паузы могут не возвращаться данным методом.',
            responses={
                200: {'description': 'Запрос удачен. Поидее любой запрос будет удачным. '
                                     'Если в базе пусто, то возвратом будет пустой список [].'
                      }
            })
async def get_queue_status(queues: Optional[List[str]] =
                           Query([], description='Массив наименований очередей, информацию по которым '
                                                 'необходимо получить. Существование не проверяется. '
                                                 'Если пусто - возвращается информация по всем очередям.')):
    logger.debug('queues == ' + str(queues))

    stmt1 = select([t_qs_queuemembers.c.queue,
                    t_qs_queuemembers.c.location,
                    func.max(t_qs_queuemembers.c.incall).label('incall'),
                    func.max(t_qs_queuemembers.c.lastcall).label('lastcall')]). \
        where(and_(t_qs_queuemembers.c.event != 'deleted',
                   t_qs_queuemembers.c.timestamp > datetime.now() -
                   timedelta(seconds=config['queue_status_out_of_date'])))

    # stmt1 = select([func.max(t_qs_queuemembers.c.timestamp).label('timestamp'),
    #                 t_qs_queuemembers.c.queue,
    #                 t_qs_queuemembers.c.location,
    #                 func.max(t_qs_queuemembers.c.lastcall).label('lastcall')]). \
    #     where(and_(t_qs_queuemembers.c.event != 'deleted',
    #                t_qs_queuemembers.c.timestamp > datetime.now() - timedelta(seconds=60)))

    if queues:
        stmt1 = stmt1.where(t_qs_queuemembers.c.queue.in_(queues))

    stmt1 = stmt1.group_by(t_qs_queuemembers.c.queue, t_qs_queuemembers.c.location).alias()
    # stmt1 = stmt1.group_by(t_qs_queuemembers.c.queue, t_qs_queuemembers.c.location).alias('a')

    j = t_qs_queuemembers.join(stmt1, and_(t_qs_queuemembers.c.timestamp == stmt1.c.timestamp,
                                           t_qs_queuemembers.c.queue == stmt1.c.queue,
                                           t_qs_queuemembers.c.location == stmt1.c.location))

    stmt = select([t_qs_queuemembers.c.timestamp,
                   t_qs_queuemembers.c.queue,
                   t_qs_queuemembers.c.location,
                   t_qs_queuemembers.c.name,
                   t_qs_queuemembers.c.paused,
                   t_qs_queuemembers.c.pausedreason,
                   t_qs_queuemembers.c.incall,
                   t_qs_queuemembers.c.status,
                   stmt1.c.lastcall]). \
        distinct(t_qs_queuemembers.c.queue, t_qs_queuemembers.c.location). \
        select_from(j).order_by(t_qs_queuemembers.c.queue,
                                t_qs_queuemembers.c.location,
                                desc(t_qs_queuemembers.c.timestamp))

    # stmt = stmt.compile(dialect=postgresql.dialect())

    print('stmt == ' + str(stmt))

    queuemembers = await db.conn.fetch_all(stmt)

    # print(str(queuemembers))

    stmt = select([t_qs_queueentries.c.queue,
                   t_qs_queueentries.c.position,
                   t_qs_queueentries.c.wait]). \
        where(and_(t_qs_queueentries.c.event != 'deleted',
                   t_qs_queueentries.c.timestamp > datetime.now() -
                   timedelta(seconds=config['queue_status_out_of_date'])))

    queueentries = {}

    for row in await db.conn.fetch_all(stmt):
        if row['queue'] not in queueentries:
            queueentries[row['queue']] = []

        queueentries[row['queue']].append(EntryModel(**row))

    # print('queueentries == ' + str(queueentries))

    queue_element: QueueModel = Undefined
    result = []

    for row in queuemembers:
        if queue_element == Undefined or queue_element.queue != row['queue']:
            queue_element = QueueModel(queue=row['queue'])
            result.append(queue_element)

            if row['queue'] in queueentries:
                result[-1].entries.extend(queueentries[row['queue']])

        # print('row == ' + str(dict(row)))
        member_element = MemberModel(**row)
        result[-1].members.append(member_element)

    # result = parse_obj_as(List[QueueModel], sql_result)

    # print(str(result))

    return result
