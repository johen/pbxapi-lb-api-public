from typing import Optional

from fastapi import APIRouter, Security
from fastapi.openapi.models import APIKey
from pydantic import BaseModel, Field
from starlette import status
from starlette.responses import Response

from ami.actions.originate import OriginateFromCRMActionData, action_originate
from core.logger import logger
from core.middlewares import get_correlation_id
from core.security import token_auth

router = APIRouter()


class CallEchoBody(BaseModel):
    dst: int = Field(..., ge=100, le=999,
                     example=311,
                     title='Внутренний номер оператора')
    async_flag: Optional[bool] = Field(True, alias='async',
                                       title='Флаг асинхронного исполнения метода')


@router.post('/make-echo-call', status_code=status.HTTP_204_NO_CONTENT,
             summary='Инициализация тестового звонка',
             description='Проверочный эхо-звонок, при котором оператор может себя послушать, проверив тем самым '
                         'свою гарнитуру и наушники')
async def make_echo_call(call_data: CallEchoBody):

    action_data = OriginateFromCRMActionData(
        actionid=str(get_correlation_id()),
        channel=str(call_data.dst),
        exten='81000',
        variable=['CALLER=' + str(call_data.dst)],
        async_flag=call_data.async_flag
    )

    logger.info('Action == ' + str(action_data.dict(exclude_none=True, by_alias=True)))

    result = await action_originate(action_data)
    logger.info('Action result == ' + str(result))

    return Response(status_code=status.HTTP_204_NO_CONTENT)


# @router.get('/')
# async def sec_test(api_key: APIKey = Security(dependency=token_auth, scopes=["crm"])):
#     return {'APIKey': str(api_key)}
