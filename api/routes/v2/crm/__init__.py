from fastapi import APIRouter
from .call_out import router as call_out_router
from .echo_call import router as echo_call_router
from .queue_status import router as queue_status_router


router = APIRouter()

router.include_router(call_out_router)
router.include_router(echo_call_router)
router.include_router(queue_status_router)
