from http import HTTPStatus

from fastapi import APIRouter, Security
from fastapi.openapi.models import APIKey
from starlette.responses import Response

from core.logger import logger
from pydantic import BaseModel, Field
from typing import Optional, List
from ami.actions.originate import OriginateFromCRMActionData, action_originate
from core.middlewares import get_correlation_id
from core.security import token_auth

router = APIRouter()


class CallOutBody(BaseModel):
    dst: int = Field(..., ge=100, le=999,
                     example=311,
                     title='Внутренний номер оператора')
    a_number: str = Field(..., min_length=10, max_length=11, regex='^[1-9][0-9]+$',
                          example='78121234567',
                          title='А-номер, с которого выполнять звонок наружу')
    b_number: str = Field(..., min_length=10, max_length=15, regex='^[1-9][0-9]+$',
                          example='79995554466',
                          title='Б-номер, номер клиента с которым будет оператор соединен')
    user_id: int = Field(...,
                         example=99999,
                         title='Идентификатор пользователя CRM')
    async_flag: Optional[bool] = Field(True, alias='async',
                                       title='Флаг асинхронного исполнения метода')
    tags: Optional[List[str]] = Field(None,
                                      example=['TAG_SG3', 'Corpses_transfer'],
                                      title='Список тегов, которые передать в запись CDR о данном звонке')


@router.post('/make-call-out', status_code=HTTPStatus.NO_CONTENT,
             summary='Инициализация исходящего звонка через CRM',
             description='Соединение указанного dst с клиентом с номером _b_number_, используя для звонка наружу '
                         'номер _a_number_. Возможна передача tags. Эти теги будут добавлены в CDR в колонку tags.')
async def make_call_out(call_data: CallOutBody):
# async def make_call_out(call_data: CallOutBody,
#                         api_key: APIKey = Security(dependency=token_auth, scopes=["crm"])):
    variables = [
        'OUTLINE=' + call_data.a_number,
        'CALLER=' + str(call_data.dst),
        'USERID=' + str(call_data.user_id)
    ]

    if call_data.tags:
        crm_tags = 'CRM_TAGS=' + '|'.join(call_data.tags)

        variables.append(crm_tags)

    action_data = OriginateFromCRMActionData(
        actionid=str(get_correlation_id()),
        channel=str(call_data.dst),
        exten=call_data.b_number,
        variable=variables,
        async_flag=call_data.async_flag
    )

    logger.info('Action == ' + str(action_data.dict(exclude_none=True, by_alias=True)))

    result = await action_originate(action_data)
    logger.info('Action result == ' + str(result))

    return Response(status_code=HTTPStatus.NO_CONTENT)
