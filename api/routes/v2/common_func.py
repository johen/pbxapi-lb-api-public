from datetime import datetime

from celery.result import AsyncResult

from core.config import config
from core.logger import logger
from core.middlewares import get_correlation_id
from rmq.connection import rmq_publisher


async def member_event_queue_log(time: datetime, event: str, **kwargs):
    """
    Функция, которая формирует данные для отправки в queue_log
    :param time: штамп времени, который нужно включить в колонку time
    :param event: ивент для колонки event
    :param kwargs: дополнительные параметры (если необходимо), которые будут добавлены/перезаписаны
    в данные для отправки
    :return:
    """
    # Дефолты
    queue_log_data = {
        'time': str(time),
        'callid': 'API',
        'queuename': 'NONE',
        'agent': 'NONE',
        'event': event,
        'data1': '',
        'data2': '',
        'data3': '',
        'data4': '',
        'data5': '',
    }

    # Добавление/перезапись данных
    queue_log_data.update(kwargs)

    await send_queue_log_to_rmq(queue_log_data)
    logger.debug('queue_log sent (' + event + ' event): ' + str(queue_log_data))


async def send_queue_log_to_rmq(data: dict) -> (AsyncResult, AsyncResult):
    """
    Вспомогательная функция для отправки data в queue_log через RMQ. Тут рутина добавления correlation_id и данных
    ноды апи. Отправка производится сразу в raw и custom лог.

    :param data: данные для отправки
    :return: результат отправки в RMQ для raw- и custom-queue_log
    """

    data_to_send = data.copy()
    data_to_send['correlation_id'] = get_correlation_id()
    data_to_send['pbxapi_node'] = config['nodename']

    try:
        raw_result = rmq_publisher.send_task('queue_log.raw',
                                             args=(data_to_send,),
                                             root_id=get_correlation_id())
        custom_result = rmq_publisher.send_task('queue_log.custom',
                                                args=(data_to_send,),
                                                root_id=get_correlation_id())

        return raw_result, custom_result

    except Exception:
        raise


async def request_queueshow_update(queuename: str) -> AsyncResult:
    """
    Отправка ивента-запроса на исполнение action command queue show для обновления нодами данных об очереди
    :param queuename: наименование очереди
    :return: результат отправки запроса в RMQ
    """

    try:
        result = rmq_publisher.send_task('ami.command.queueshow.request',
                                         args=(queuename, config['nodename'],),
                                         countdown=config['queueshow_update_countdown'],
                                         root_id=get_correlation_id())

        logger.debug('Queueshow update request sent with task_id == ' + str(result))

        return result

    except Exception:
        raise
