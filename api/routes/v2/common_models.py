import os
from enum import Enum
from typing import Optional
from uuid import UUID

from pydantic import BaseModel, Field, validator
from pydantic.fields import Undefined

from core.config import config
from core.middlewares import get_correlation_id


class QueueMemberEvent(str, Enum):
    added = 'added'
    removed = 'deleted'


class QueueMember(BaseModel):
    queue_name: str = Field(..., min_length=1, max_length=80,
                            title='Наименование очереди.')
    interface: str = Field(..., min_length=1, max_length=80,
                           regex='^(?!' + config['channel_driver'] + '/).+',
                           title='Наименование интерфейса.')
    membername: Optional[str] = Field(Undefined, min_length=1, max_length=80,
                                      title='Наименование члена очереди.')
    state_interface: Optional[str] = Field(Undefined, min_length=1, max_length=80)
    penalty: Optional[int] = Field(0, ge=0)
    paused: Optional[bool] = Field(False)
    wrapuptime: Optional[int] = Field(0, ge=0)
    static_membership: Optional[bool] = Field(False,
                                              title='Фактор динамического членства в очереди')
    event: QueueMemberEvent = Field(QueueMemberEvent.added)
    correlation_id: UUID = Field(default_factory=get_correlation_id)
    nodename: str = Field(config['nodename'])

    @validator('interface')
    def update_with_technology(cls, v):
        return config['channel_driver'] + '/' + v

    @validator('membername', always=True)
    def update_user_id(cls, v, values, **kwargs):
        if v:
            return values['interface'] + ' [user_id:' + v + ']'
        else:
            return values['interface']

    @validator('state_interface', always=True)
    def update_state_interface(cls, v, values, **kwargs):
        if v:
            return v
        else:
            return values['interface']
