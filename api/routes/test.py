from databases import Database
from database.tables_security import t_users, t_group_members
from sqlalchemy import select, and_

db = Database('postgresql://pbxapi_rw:1QaZ2WsX@localhost/pbxapi')


user = 'crm'
token = '53fdb865-4f43-485b-a445-c1bb41f9875a'


async def get_data(user, token):
    await db.connect()
    stmt = select([t_group_members.c.groupname]).where(
        and_(
            t_users.c.username == user,
            t_users.c.token == token
        )
    )

    async for row in db.fetch_one(stmt).iterate():
        print(str(row))


get_data(user, token)

# print(str(result))
