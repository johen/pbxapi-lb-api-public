from starlette.requests import Request
from core.logger import logger


async def request_data_back_logger(request: Request):
    logger.error('Request data following')
    for k, v in request.items():
        logger.error(str(k) + ' == ' + str(v))


async def exception_data_back_logger(exc: Exception):
    logger.error('Exception data following')

    if hasattr(exc, 'body'):
        logger.error('body == ' + str(exc.body))
    if hasattr(exc, 'errors'):
        logger.error('errors == ' + str(exc.errors))
    if hasattr(exc, 'detail'):
        logger.error('detail == ' + str(exc.detail))

    logger.error('Exception type: ' + str(type(exc)))
    logger.error('Exception repr data: ' + str(repr(exc)))


async def back_logger(log_string: str):
    logger.error(log_string)
