from typing import Union

from fastapi.exceptions import RequestValidationError
from pydantic import ValidationError
from starlette.background import BackgroundTasks
from starlette.exceptions import HTTPException as StarletteHTTPException
from starlette.requests import Request
from starlette.responses import JSONResponse
from starlette.status import HTTP_422_UNPROCESSABLE_ENTITY, HTTP_500_INTERNAL_SERVER_ERROR

from core.background_tasks import request_data_back_logger, exception_data_back_logger, back_logger
from core.logger import logger
from ami.manager import AMI


async def http422_error_handler(
        request: Request, exc: Union[RequestValidationError, ValidationError]
) -> JSONResponse:
    tasks = BackgroundTasks()
    tasks.add_task(request_data_back_logger, request)
    tasks.add_task(exception_data_back_logger, exc)
    tasks.add_task(back_logger, 'Returned code ' + str(HTTP_422_UNPROCESSABLE_ENTITY))
    return JSONResponse(
        {'detail': exc.errors()}, status_code=HTTP_422_UNPROCESSABLE_ENTITY, background=tasks
    )


async def nolivenodes_error_handler(request: Request, exc: AMI.ErrorNoLiveNode) -> JSONResponse:
    tasks = BackgroundTasks()
    tasks.add_task(request_data_back_logger, request)
    tasks.add_task(exception_data_back_logger, exc)
    tasks.add_task(back_logger, 'Returned code 500')
    return JSONResponse(
        {'detail': 'No connection to live node'}, status_code=HTTP_500_INTERNAL_SERVER_ERROR, background=tasks
    )


async def httpexception_error_handler(request: Request, exc: StarletteHTTPException) -> JSONResponse:
    tasks = BackgroundTasks()
    tasks.add_task(request_data_back_logger, request)
    tasks.add_task(exception_data_back_logger, exc)
    tasks.add_task(back_logger, 'Returned code ' + str(exc.status_code))
    return JSONResponse(
        {'detail': exc.detail}, status_code=exc.status_code, background=tasks
    )


async def abnormal_exception_handler(request: Request, exc: Exception) -> JSONResponse:
    logger.error('ABNORMAL EXCEPTION RAISED!')
    tasks = BackgroundTasks()
    tasks.add_task(request_data_back_logger, request)
    tasks.add_task(exception_data_back_logger, exc)

    tasks.add_task(back_logger, 'Returned code ' + str(HTTP_500_INTERNAL_SERVER_ERROR))

    return JSONResponse(
        {'detail': 'Abnormal exception raised'}, status_code=HTTP_500_INTERNAL_SERVER_ERROR, background=tasks
    )
