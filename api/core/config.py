import yaml


class Config:
    def __init__(self):
        self._config_options = None

    def __getitem__(self, item):
        return self._config_options[item]

    def load(self, config_file):
        try:
            with open(config_file, 'r') as f:
                self._config_options = yaml.safe_load(f)

        except Exception as err:
            raise self.UnableToLoadConfiguration(err)

    class UnableToLoadConfiguration(Exception):
        # Ошибка вызывается, если происходит какая-то проблема с загрузкой данных из конфигурационного файла
        pass


# Текущая конфигурация
config = Config()
