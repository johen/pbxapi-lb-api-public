import logging
from core.middlewares import get_request_id, get_correlation_id


class CorrelationIdFormatter(logging.Formatter):
    """
    Форматтер, добавляющий correlation_id и request_id в список переменных для форматирования
    """
    def format(self, record: logging.LogRecord) -> str:
        record.correlation_id = get_correlation_id()
        record.request_id = get_request_id()

        return super().format(record)


class ExcludeWarningFilter(logging.Filter):
    """
    Фильтр, отсекающий уровень warning, error и далее
    """
    def filter(self, record: logging.LogRecord) -> bool:
        return record.levelno < logging.WARNING


class IncludeWarningFilter(logging.Filter):
    """
    Фильтр, отсекающий уровени DEBUG и INFO
    """
    def filter(self, record: logging.LogRecord) -> bool:
        return record.levelno >= logging.WARNING
