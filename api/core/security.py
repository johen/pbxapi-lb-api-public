from typing import List

from aiocache import cached
from fastapi import Depends
from fastapi.security import SecurityScopes
from fastapi.security.api_key import APIKeyHeader
from sqlalchemy import select, and_
from starlette.exceptions import HTTPException
from starlette.status import HTTP_403_FORBIDDEN

from core.logger import logger
from database.connection import db
from database.tables_security import t_users, t_group_members

api_key_header = APIKeyHeader(name='X-Auth-Token')


@cached(ttl=30)
async def get_current_user(scopes: List[str], token: str):
    stmt = select([t_users.c.username]). \
        where(and_(t_users.c.username == t_group_members.c.username,
                   t_users.c.token == token,
                   t_group_members.c.groupname.in_(scopes)))

    result = await db.conn.fetch_one(query=stmt)

    return result['username'] if result is not None else None


async def token_auth(security_scopes: SecurityScopes, api_key: str = Depends(api_key_header)):
    try:
        username, token = api_key.split(':')
    except ValueError:
        logger.error('Unable to split api_key')
        raise HTTPException(status_code=HTTP_403_FORBIDDEN, detail='Permission denied')

    user_in_db = await get_current_user(security_scopes.scopes, token)

    if user_in_db is None or user_in_db != username:
        raise HTTPException(status_code=HTTP_403_FORBIDDEN, detail='Permission denied')

    return api_key_header
