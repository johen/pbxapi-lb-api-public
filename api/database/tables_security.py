from sqlalchemy import Column, ForeignKey, MetaData, String, Table

from core.config import config

# Просто сокращение
_current_schema = config['database']['api_security_schema']

metadata = MetaData(schema=_current_schema)

t_groups = Table(
    'groups', metadata,
    Column('groupname', String(64), primary_key=True, unique=True),
    Column('description', String(256))
)

t_users = Table(
    'users', metadata,
    Column('username', String(64), primary_key=True, unique=True),
    Column('token', String(256), nullable=False),
    Column('description', String(256))
)

t_group_members = Table(
    'group_members', metadata,
    Column('groupname', ForeignKey(_current_schema + '.groups.groupname', ondelete='RESTRICT', onupdate='CASCADE'),
           primary_key=True, nullable=False),
    Column('username', ForeignKey(_current_schema + '.users.username', ondelete='RESTRICT', onupdate='CASCADE'),
           primary_key=True, nullable=False),
    Column('description', String(256))
)
