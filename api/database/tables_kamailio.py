from sqlalchemy import Column, Integer, MetaData, String, Table, UniqueConstraint, text

metadata = MetaData(schema='public')

t_subscriber = Table(
    'subscriber', metadata,
    Column('id', Integer, primary_key=True, server_default=text("nextval('\"public\".subscriber_id_seq'::regclass)")),
    Column('username', String(64), nullable=False, index=True, server_default=text("''::character varying")),
    Column('domain', String(64), nullable=False, server_default=text("''::character varying")),
    Column('password', String(64), nullable=False, server_default=text("''::character varying")),
    Column('ha1', String(128), nullable=False, server_default=text("''::character varying")),
    Column('ha1b', String(128), nullable=False, server_default=text("''::character varying")),
    UniqueConstraint('username', 'domain'),
)
