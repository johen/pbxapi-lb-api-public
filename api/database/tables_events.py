from sqlalchemy import Table, MetaData, Column, String, DateTime, Integer, text, Float
from sqlalchemy.dialects.postgresql import UUID, ENUM

from core.config import config

# Просто сокращение
_current_schema = config['database']['events_schema']

metadata = MetaData(schema=_current_schema)
# metadata = MetaData(schema='cluster_events3')

channels_table = Table(
    'channels', metadata,

    Column('nodename', String(64), primary_key=True, nullable=False),
    Column('event', String(64), nullable=False),
    Column('timestamp', DateTime, nullable=False),

    Column('channel', String(64), primary_key=True, nullable=False),
    Column('uniqueid', String(64), primary_key=True, nullable=False),
    Column('channelstate', Integer),
    Column('channelstatedesc', String(64)),
    Column('calleridnum', String(64)),
    Column('calleridname', String(64)),
    Column('connectedlinenum', String(64)),
    Column('connectedlinename', String(64)),
    Column('language', String(2)),
    Column('accountcode', String(64)),
    Column('context', String(64)),
    Column('exten', String(64)),
    Column('priority', String(64)),
    Column('linkedid', String(64)),
    Column('cause', Integer),
    Column('cause_txt', String(64)),

    Column('correlation_id', UUID(as_uuid=True), nullable=False)
)

variables_table = Table(
    'variables', metadata,

    Column('nodename', String(64), primary_key=True, nullable=False),
    Column('event', String(64), nullable=False),
    Column('timestamp', DateTime, nullable=False),

    Column('channel', String(64), primary_key=True, nullable=False),
    Column('uniqueid', String(64), primary_key=True, nullable=False),
    Column('channelstate', Integer),
    Column('channelstatedesc', String(64)),
    Column('calleridnum', String(64)),
    Column('calleridname', String(64)),
    Column('connectedlinenum', String(64)),
    Column('connectedlinename', String(64)),
    Column('language', String(2)),
    Column('accountcode', String(64)),
    Column('context', String(64)),
    Column('exten', String(64)),
    Column('priority', String(64)),
    Column('linkedid', String(64)),
    Column('variable', String(64), primary_key=True, nullable=False),
    Column('value', String(256)),

    Column('correlation_id', UUID(as_uuid=True), nullable=False)
)


agentconnections_table = Table(
    'agentconnections', metadata,

    Column('nodename', String(64), primary_key=True, nullable=False),
    Column('event', String(64), nullable=False),
    Column('timestamp', DateTime, nullable=False),

    Column('channel', String(64), primary_key=True, nullable=False),
    Column('uniqueid', String(64), primary_key=True, nullable=False),
    Column('channelstate', Integer),
    Column('channelstatedesc', String(64)),
    Column('calleridnum', String(64)),
    Column('calleridname', String(64)),
    Column('connectedlinenum', String(64)),
    Column('connectedlinename', String(64)),
    Column('language', String(2)),
    Column('accountcode', String(64)),
    Column('context', String(64)),
    Column('exten', String(64)),
    Column('priority', String(64)),
    Column('linkedid', String(64)),

    Column('destchannel', String(64), nullable=False),
    Column('destuniqueid', String(64), nullable=False),
    Column('destchannelstate', Integer),
    Column('destchannelstatedesc', String(64)),
    Column('destcalleridnum', String(64)),
    Column('destcalleridname', String(64)),
    Column('destconnectedlinenum', String(64)),
    Column('destconnectedlinename', String(64)),
    Column('destlanguage', String(2)),
    Column('destaccountcode', String(64)),
    Column('destcontext', String(64)),
    Column('destexten', String(64)),
    Column('destpriority', String(64)),
    Column('destlinkedid', String(64)),

    Column('queue', String(64), primary_key=True, nullable=False),
    Column('membername', String(64)),
    Column('interface', String(64), primary_key=True, nullable=False),
    Column('ringtime', Integer),
    Column('holdtime', Integer),
    Column('talktime', Integer),
    Column('reason', String(64)),

    Column('correlation_id', UUID(as_uuid=True), nullable=False)
)


bridges_table = Table(
    'bridges', metadata,

    Column('nodename', String(64), primary_key=True, nullable=False),
    Column('event', String(64), nullable=False),
    Column('timestamp', DateTime, nullable=False),

    Column('bridgeuniqueid', UUID(as_uuid=True), primary_key=True, nullable=False),
    Column('bridgetype', String(64), nullable=False),
    Column('bridgetechnology', String(64), nullable=False),
    Column('bridgecreator', String(64)),
    Column('bridgename', String(64)),
    Column('bridgenumchannels', Integer),
    Column('bridgevideosourcemode', String(64)),
    Column('bridgevideosource', String(64)),

    Column('correlation_id', UUID(as_uuid=True), nullable=False)
)


bridgemembership_table = Table(
    'bridgemembership', metadata,

    Column('nodename', String(64), primary_key=True, nullable=False),
    Column('event', String(64), nullable=False),
    Column('timestamp', DateTime, nullable=False),

    Column('channel', String(64), primary_key=True, nullable=False),
    Column('uniqueid', String(64), primary_key=True, nullable=False),
    Column('channelstate', Integer),
    Column('channelstatedesc', String(64)),
    Column('calleridnum', String(64)),
    Column('calleridname', String(64)),
    Column('connectedlinenum', String(64)),
    Column('connectedlinename', String(64)),
    Column('language', String(2)),
    Column('accountcode', String(64)),
    Column('context', String(64)),
    Column('exten', String(64)),
    Column('priority', String(64)),
    Column('linkedid', String(64)),

    Column('bridgeuniqueid', UUID(as_uuid=True), primary_key=True, nullable=False),
    Column('bridgetype', String(64), nullable=False),
    Column('bridgetechnology', String(64), nullable=False),
    Column('bridgecreator', String(64)),
    Column('bridgename', String(64)),
    Column('bridgenumchannels', Integer),
    Column('bridgevideosourcemode', String(64)),
    Column('bridgevideosource', String(64)),
    Column('swapuniqueid', String(64)),

    Column('correlation_id', UUID(as_uuid=True), nullable=False)
)


queuecallers_table = Table(
    'queuecallers', metadata,

    Column('nodename', String(64), primary_key=True, nullable=False),
    Column('event', String(64), nullable=False),
    Column('timestamp', DateTime, nullable=False),

    Column('channel', String(64), primary_key=True, nullable=False),
    Column('uniqueid', String(64), primary_key=True, nullable=False),
    Column('channelstate', Integer),
    Column('channelstatedesc', String(64)),
    Column('calleridnum', String(64)),
    Column('calleridname', String(64)),
    Column('connectedlinenum', String(64)),
    Column('connectedlinename', String(64)),
    Column('language', String(2)),
    Column('accountcode', String(64)),
    Column('context', String(64)),
    Column('exten', String(64)),
    Column('priority', String(64)),
    Column('linkedid', String(64)),

    Column('queue', String(64), primary_key=True, nullable=False),
    Column('position', Integer, nullable=False),
    Column('count', Integer, nullable=False),

    Column('correlation_id', UUID(as_uuid=True), nullable=False)
)


queuememberstatus_table = Table(
    'queuememberstatus', metadata,

    Column('nodename', String(64), primary_key=True, nullable=False),
    Column('event', String(64), nullable=False),
    Column('timestamp', DateTime, nullable=False),

    Column('queue', String(64), primary_key=True, nullable=False),
    Column('membername', String(64)),
    Column('interface', String(64), primary_key=True, nullable=False),
    Column('stateinterface', String(64)),
    Column('membership', String(64)),
    Column('penalty', Integer),
    Column('callstaken', Integer),
    Column('lastcall', DateTime),
    Column('lastpause', DateTime),
    Column('incall', Integer, nullable=False),
    Column('status', Integer, nullable=False),
    Column('paused', Integer, nullable=False),
    Column('pausedreason', String(64)),
    Column('ringinuse', Integer, nullable=False),
    Column('wrapuptime', Integer),

    Column('correlation_id', UUID(as_uuid=True), nullable=False)
)


t_qs_queueentries = Table(
    'qs_queueentries', metadata,
    Column('pbxapi_node', String(64), primary_key=True, nullable=False),
    Column('timestamp', DateTime, nullable=False),
    Column('event', ENUM('inserted', 'updated', 'deleted', name='data_change_event'), nullable=False,
           server_default=text("'inserted'::" + _current_schema + ".data_change_event")),

    Column('queue', String(64), primary_key=True, nullable=False),
    Column('position', Integer),
    Column('channel', String(64), primary_key=True, nullable=False),
    Column('uniqueid', String(64), primary_key=True, nullable=False),
    Column('calleridnum', String(64)),
    Column('calleridname', String(64)),
    Column('connectedlinenum', String(64)),
    Column('connectedlinename', String(64)),
    Column('wait', Integer),
    Column('priority', Integer),

    Column('correlation_id', UUID(as_uuid=True), nullable=False)
)

t_qs_queuemembers = Table(
    'qs_queuemembers', metadata,
    Column('pbxapi_node', String(64), primary_key=True, nullable=False),
    Column('timestamp', DateTime, nullable=False),
    Column('event', ENUM('inserted', 'updated', 'deleted', name='data_change_event'), nullable=False,
           server_default=text("'inserted'::" + _current_schema + ".data_change_event")),

    Column('queue', String(64), primary_key=True, nullable=False),
    Column('name', String(64)),
    Column('location', String(64), primary_key=True, nullable=False),
    Column('stateinterface', String(64)),
    Column('membership', String(64)),
    Column('penalty', Integer),
    Column('callstaken', Integer),
    Column('lastcall', DateTime),
    Column('lastpause', DateTime),
    Column('incall', Integer, nullable=False),
    Column('status', Integer, nullable=False),
    Column('paused', Integer, nullable=False),
    Column('pausedreason', String(64)),
    Column('wrapuptime', Integer),

    Column('correlation_id', UUID(as_uuid=True), nullable=False)
)

t_qs_queueparams = Table(
    'qs_queueparams', metadata,
    Column('pbxapi_node', String(64), primary_key=True, nullable=False),
    Column('timestamp', DateTime, nullable=False),
    Column('event', ENUM('inserted', 'updated', 'deleted', name='data_change_event'), nullable=False,
           server_default=text("'inserted'::" + _current_schema + ".data_change_event")),

    Column('queue', String(64), primary_key=True, nullable=False),
    Column('max', Integer),
    Column('strategy', String(64)),
    Column('calls', Integer),
    Column('holdtime', Integer),
    Column('talktime', Integer),
    Column('completed', Integer),
    Column('abandoned', Integer),
    Column('servicelevel', Integer),
    Column('servicelevelperf', Float(53)),
    Column('servicelevelperf2', Float(53)),
    Column('weight', Integer),

    Column('correlation_id', UUID(as_uuid=True), nullable=False)
)
