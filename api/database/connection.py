from databases import Database
from pydantic import typing
from sqlalchemy.sql import ClauseElement


class DB(Database):
    """
    Класс дочерний для перегрузки методов, которые выполняют попытки переподключения при возникновении разрыва
    """
    async def fetch_one(
        self, query: typing.Union[ClauseElement, str], values: dict = None
    ) -> typing.Optional[typing.Mapping]:
        try:
            return await super(DB, self).fetch_one(query, values)
        except (ConnectionRefusedError, AssertionError):
            try:
                await self.connect()
                return await super(DB, self).fetch_one(query, values)
            except Exception as err:
                raise DBConnectionError(err)


class DBConnectionError(Exception):
    """
    Ошибка кастомная, чтобы унифицировать обработку в exception_handlers fastapi
    """
    pass


class DBWrapper:
    """
    Элементарный класс-враппер для того, чтобы создать элемент класса ниже без необходимости сразу предоставить конфиг
    """
    def __init__(self):
        self.conn: DB = None

    def from_config(self, config):
        try:
            self.conn = DB(config['url'])
        except Exception:
            raise


# Текущие подключения
# Подключение к БД pbxapi
db = DBWrapper()
# Подключение к БД webrtc-гейта kamailio
db_webrtc = DBWrapper()
# Подключение к БД ARA
db_ara = DBWrapper()
