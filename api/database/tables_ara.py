from sqlalchemy import Column, Integer, MetaData, String, Table, text, Enum, Boolean
from sqlalchemy.dialects.postgresql import UUID

from core.config import config

metadata = MetaData(schema=config['database_ara']['schema'])

t_queue_members = Table(
    'queue_members', metadata,
    Column('queue_name', String(80), primary_key=True, nullable=False),
    Column('interface', String(80), primary_key=True, nullable=False),
    Column('membername', String(80)),
    Column('state_interface', String(80)),
    Column('penalty', Integer),
    Column('paused', Integer),
    Column('uniqueid', Integer, nullable=False, unique=True,
           server_default=text("nextval('queue_members_uniqueid_seq'::regclass)")),
    Column('wrapuptime', Integer),
    Column('static_membership', Boolean, nullable=False, server_default=text("false")),
    Column('event', Enum('added', 'deleted', name='queue_members_event_values'), nullable=False,
           server_default=text("'added'::queue_members_event_values")),
    Column('correlation_id', UUID),
    Column('nodename', String(64))
)
