from functools import lru_cache
from pydantic import BaseConfig


class Settings(BaseConfig):
    app_name: str = 'pbxapi-lb'


@lru_cache()
def get_settings():
    return Settings()


settings = get_settings()
