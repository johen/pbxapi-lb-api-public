import os
from core.config import config

# Наличие подгрузки конфига именно выше остальных импортов важно, чтобы можно было бы использовать config в них
config.load(os.environ['CONFIG_FILE'])

import traceback

from fastapi import FastAPI
from core.logger import logger
from ami.manager import ami

from core.middlewares import RequestContextLogMiddleware
from routes.v1 import app as v1_app
from routes.v2 import app as v2_app
from openapi_data import title, description, version, tags, servers
from database.connection import db, db_webrtc, db_ara

app = FastAPI(
    title=title,
    description=description,
    version=version,
    openapi_tags=tags,
    servers=servers
)


@app.on_event('startup')
async def initialize_ami_managers():
    logger.info('Startup event catched! Initializing connection to asterisk hosts...')
    try:
        ami.config(config['ami'])
        ami.initialize()

        # TODO: Тут необходимо добавить обработку ошибок, типа неверный пароль с последующей например остановкой
        #  uvicorn
        db.from_config(config['database'])
        await db.conn.connect()

        db_webrtc.from_config(config['database_webrtc'])
        await db_webrtc.conn.connect()

        db_ara.from_config(config['database_ara'])
        await db_ara.conn.connect()
    except Exception as err:
        logger.error('Something happened while initializing panoramisk manager or connecting to DB: ' + str(err))
        logger.debug('Traceback:\n' + traceback.format_exc())
    else:
        logger.info('Connected')


@app.on_event('shutdown')
async def stop_ami_managers():
    logger.info('Shutdown event catched! Shutting down connection to asterisk hosts...')
    try:
        ami.close()
        await db.conn.disconnect()
        await db_webrtc.conn.disconnect()
        await db_ara.conn.disconnect()
    except Exception as err:
        logger.error('Something happened while shutting down panoramisk manager and connection to DB: ' + str(err))
        logger.debug('Traceback:\n' + traceback.format_exc())
    else:
        logger.info('Manager disconnected')


app.add_middleware(RequestContextLogMiddleware)

# routers
app.mount('/v1', v1_app)
app.mount('/v2', v2_app)

# events
# middleware
