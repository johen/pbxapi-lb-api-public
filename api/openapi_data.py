from typing import List

# Общая информация для OAS

title = 'PBX API for CRM'
description = 'API для взаимодействия кластера Asterisk в режиме Realtime и CRM'
version = '1.0'
tags = [
    {
        'name': 'channels',
        'description': 'Методы манипуляции с каналами',
    },
    {
        'name': 'queues',
        'description': 'Методы взаимодействия с очередями',
    },
    {
        'name': 'calls',
        'description': 'Методы оригинации звонков',
    },
    {
        'name': 'redirect',
        'description': 'Методы переадресации',
    },
    {
        'name': 'endpoint',
        'description': 'Методы работы с конечными endpoint\'ами',
    },
    {
        'name': 'crm',
        'description': 'Специальные методы, заказанные CRM',
    },
    {
        'name': 'webrtc',
        'description': 'Методы взаимодействия с гейтом webrtc',
    }
]
servers = [
    {'url': 'http://localhost:8000'}
]
