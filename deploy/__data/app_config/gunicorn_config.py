import multiprocessing
import sys

bind = 'unix:{{ app_socket_path }}'
# bind = '127.0.0.1:8000'
workers = multiprocessing.cpu_count()
worker_class = 'uvicorn.workers.UvicornWorker'
# worker_connections = 1000
# timeout = 30
# keepalive = 2
# spew = False
# daemon = True
# pidfile = '/run/pbxapi_lb_api-gunicorn.pid'
user = '{{ owner }}'
group = '{{ group }}'
tmp_upload_dir = None
proc_name = '{{ app_name }}_gunicorn'

log_level = '{{ gunicorn_config_log_level }}'
log_level_panoramisk = '{{ gunicorn_config_log_level_panoramisk }}'
# chdir =
raw_env = [
  'PYTHONUNBUFFERED={{ gunicorn_config_raw_env_pythonunbuffered }}',
  'CONFIG_FILE=../{{ app_config_path }}',
  'PYTHONASYNCIODEBUG={{ gunicorn_config_raw_env_pythonasynciodebug }}',
  'DEBUG={{ gunicorn_config_raw_env_debug }}'
]


logconfig_dict = {
  'version': 1,
  'formatters': {
    'console': {
      '()': 'api.core.logger_formatters.CorrelationIdFormatter',
      'format': '%(asctime)s PBXAPI-LB-API-%(name)s [%(process)d] [%(request_id)s] [%(correlation_id)s] %(levelname)s: %(message)s'
    },
    'syslog': {
      '()': 'api.core.logger_formatters.CorrelationIdFormatter',
      'format': 'PBXAPI-LB-API-%(name)s [%(process)d] [%(request_id)s] [%(correlation_id)s] %(levelname)s: %(message)s'
    }
  },
  'filters': {
    'exclude_warning': {
      '()': 'api.core.logger_formatters.ExcludeWarningFilter',
    },
    'include_warning': {
      '()': 'api.core.logger_formatters.IncludeWarningFilter',
    },
  },
  'handlers': {
    'console': {
      'class': 'logging.StreamHandler',
      'formatter': 'console',
    },
    'console_stdout': {
      'class': 'logging.StreamHandler',
      'formatter': 'console',
      'filters': ['exclude_warning'],
      'stream': sys.stdout,
    },
    'console_stderr': {
      'class': 'logging.StreamHandler',
      'formatter': 'console',
      'filters': ['include_warning'],
      'stream': sys.stderr,
    },
    'syslog': {
      'class': 'logging.handlers.SysLogHandler',
      'address': '/dev/log',
      'formatter': 'syslog',
    }
  },
  'loggers': {
    'generic': {
      'level': log_level,
      'handlers': [
        'syslog'
        # 'console_stdout', 'console_stderr', 'syslog'
      ],
      'propagate': False,
    },
    'gunicorn': {
      'level': log_level,
      'handlers': [
        'syslog'
        # 'console_stdout', 'console_stderr', 'syslog'
      ],
      'propagate': False,
    },
    'gunicorn.access': {
      'level': log_level,
      'handlers': [
        'syslog'
        # 'console_stdout', 'console_stderr', 'syslog'
      ],
      'propagate': False,
    },
    'panoramisk': {
      'level': log_level_panoramisk,
      'handlers': [
        'syslog'
        # 'console_stdout', 'console_stderr', 'syslog'
      ],
      'propagate': False,
    },
    'root': {
      'level': log_level,
      'handlers': [
        'syslog'
        # 'console'
      ],
      'propagate': False,
    }
  }
}